import React from 'react';
import {
  Animated,
  View,
  Image,
  ImageBackground,
  Platform,
  Alert,
  ActivityIndicator,
} from 'react-native';

import Slider from '@react-native-community/slider';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Video from 'react-native-video';
import LinearGradient from 'react-native-linear-gradient';
import GoogleCast from 'react-native-google-cast';
import {PulseIndicator} from 'react-native-indicators';

import {ActiveCompanyContext} from '../providers/CompanyProvider';
import Routes from '../environments/Routes';
import * as Network from '../network';
import Colors from '../environments/Colors';
import {Modal} from './';
import {AppText, Touchable} from './common';

// Note: VideoPlayer class assume you leave him the control of its size to render the video ratio properly,
// it assume you leave him full width of the screen
// To add margins to video and its overlay, modify the player params in componentWillMount()

// props
// id -- used to update the server on user play / pause
// uri -- uri of the media
// live -- if it's a live or an onDemand video
// started -- if the live is already started, if onDemand == 1
// fullscreen
// viewers -- times the video have been seen

class VideoPlayer extends React.Component {
  static contextType = ActiveCompanyContext;
  tvButtons = [];
  currentButton = 1;
  state = {
    currentTime: 0,
    duration: 0,
    paused: true,
    loadingVideo: true,
    videoError: false,
    castConnected: false,
    casting: false,
    castModal: false,
    isTv: false,
    end: 1,
  };
  action = '';
  video: Video;
  onBuffer = data => {
    // this.setState({ duration: data.duration });
  };
  onLoad = data => {
    let videoRatio = data.naturalSize.width / data.naturalSize.height;
    let stateSetup = {
      videoWidth: data.naturalSize.width,
      videoHeight: data.naturalSize.height,
      videoWHRatio: videoRatio,

      portraitHeight: global.screenProp.width / videoRatio,
      portraitWidth: global.screenProp.width,
      landscapeHeight: global.screenProp.width,
      landscapeWidth: global.screenProp.width * videoRatio,
      landscapeMarginLeft:
        (global.screenProp.height - global.screenProp.width * videoRatio) / 2,
      landscapeMarginRight:
        (global.screenProp.height - global.screenProp.width * videoRatio) / 2,
    };

    if (Platform.OS == 'android') {
      const playerMargin =
        global.screenProp.height - 48 - global.screenProp.width * videoRatio;
      if (playerMargin == 0) {
        // video width == screenWidth
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: 0,
          landscapeMarginRight: 0,
        };
      } else if (playerMargin > 0) {
        // else arrange the video to not be under the navbar
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: playerMargin / 2,
          landscapeMarginRight: 48 + playerMargin / 2,
        };
      }
    }
    this.video.seek(0);
    this.playerOpacityTimer();
    this.setAction();
    this.setUpdateInterval();
    // this.focusPause();
    this.setState(
      {
        duration: data.duration ? data.duration : 0,
        loadingVideo: false,
        // ...stateSetup,
      },
      () => this.setState({castModal: this.state.castConnected, paused: false}),
    );
  };

  async setAction() {
    this.action = await Network.playVideo(this.props.id, 0);
  }

  focusPause() {
    if (Platform.isTV) {
      if (this.playPauseButton) {
        this.playPauseButton.props.onPress();
        // this.playPauseButton.current.focus();
      } else {
        setTimeout(() => this.focusPause(), 1500);
      }
    }
  }

  setUpdateInterval() {
    this.updateInterval = setInterval(() => this.updateVideo(), 15000);
  }
  stopUpdateInterval() {
    clearInterval(this.updateInterval);
    this.updateInterval = null;
  }

  onLoadStart = data => {
    this.setState({loadingVideo: true});
  };
  onProgress = data => {
    this.props.onProgressTime(data.currentTime);
    this.setState({currentTime: data.currentTime});
  };
  onEnd = () => {
    this.setState({end: 1});
    this.updateVideo(parseInt(this.state.duration));
    this.setState({paused: true, currentTime: 0});
    this.video.seek(0);
    this.stopUpdateInterval();
  };
  onSeek = data => console.log('seeking', data);
  onError = error => {
    this.setState({videoError: true, loadingVideo: false});
  };

  cast() {
    const {title} = this.props;
    this.setState({paused: true, casting: true, castModal: false});
    if (!Platform.isTV) {
      GoogleCast.castMedia({
        mediaUrl: this.props.uri,
        imageUrl: this.context.activeCompany.logo,
        title: title,
        // subtitle:
        //   info.description,
        // studio: 'Surf Evolution',
        streamDuration: this.state.duration, // seconds
        playPosition: this.state.currentTime, // seconds
      });
      GoogleCast.launchExpandedControls();
      GoogleCast.play();
      this.setState({castModal: false});
    }
  }

  castSetup() {
    const events = `
      SESSION_STARTING SESSION_STARTED SESSION_START_FAILED SESSION_SUSPENDED
      SESSION_RESUMING SESSION_RESUMED SESSION_ENDING SESSION_ENDED
      MEDIA_STATUS_UPDATED MEDIA_PLAYBACK_STARTED MEDIA_PLAYBACK_ENDED MEDIA_PROGRESS_UPDATED
      CHANNEL_CONNECTED CHANNEL_DISCONNECTED CHANNEL_MESSAGE_RECEIVED
    `
      .trim()
      .split(/\s+/);
    if (!Platform.isTV) {
      // listener forEach event in events
      events.forEach(event => {
        GoogleCast.EventEmitter.addListener(GoogleCast[event], () => {
          // GoogleCast.getCastState().then(response => response === 'Connected' && handleClicked(event))
          if (event === 'SESSION_STARTED') {
            this.setState({castConnected: true}, () => {
              if (!this.state.loadingVideo) {
                this.setState({castModal: true});
              }
            });
          } else if (event === 'SESSION_ENDED') {
            this.setState({castConnected: false});
          }
        });
      });
      GoogleCast.getCastState().then(state =>
        this.setState({castConnected: state == 'Connected' ? true : false}),
      );
    } else {
      this.setState({isTv: true});
    }
  }

  seek(seconds) {
    this.video.seek(seconds);
    this.setState({paused: false});
  }

  componentWillMount() {
    const {started, uri} = this.props;
    this.overlayOpacity = new Animated.Value(1);
    this.action = '';
    let stateSetup = {
      duration: 0,
      currentTime: 0,
      paused: true,
      loadingVideo: true,
      portraitHeight: (global.screenProp.width / 16) * 9,
      portraitWidth: global.screenProp.width,
      landscapeHeight: global.screenProp.width,
      landscapeWidth: (global.screenProp.width / 9) * 16,
    };

    if (Platform.OS == 'android') {
      // TODO: check if softNavBar is present
      const playerMargin =
        global.screenProp.height - 48 - (global.screenProp.width / 9) * 16;
      if (playerMargin == 0) {
        // video width == screenWidth
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: 0,
          landscapeMarginRight: 0,
        };
      } else if (playerMargin > 0) {
        // else arrange the video to not be under the navbar
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: playerMargin / 2,
          landscapeMarginRight: 48 + playerMargin / 2,
        };
      }
    } else {
      // if iOS or no soft navbar is present
      stateSetup = {
        ...stateSetup,
        landscapeMarginLeft:
          (global.screenProp.height - (global.screenProp.width / 9) * 16) / 2,
        landscapeMarginRight:
          (global.screenProp.height - (global.screenProp.width / 9) * 16) / 2,
      };
    }
    // if the video is a live not started yet, remove the loading feedback
    // from the overlay with the loadingVideo state to false
    this.setState({...stateSetup, loadingVideo: !!started}, () =>
      this.castSetup(),
    );
    // Network.updateVideo(this.props.id, parseInt(this.state.currentTime), this.action)
  }

  componentWillUnmount() {
    this.updateVideo();
    if (this.state.casting && !Platform.isTV) {
      GoogleCast.stop();
      GoogleCast.endSession();
    }
    if (this.updateInterval) {
      clearInterval(this.updateInterval);
    }
  }

  playerOpacityTimer() {
    Animated.timing(this.overlayOpacity, {
      toValue: 1,
      duration: 150,
      useNativeDriver: false,
    }).start(() => {
      if (!this.state.paused) {
        Animated.timing(this.overlayOpacity, {
          toValue: 0,
          duration: 150,
          delay: 3500,
          useNativeDriver: false,
        }).start();
      }
    });
  }

  async playVideo(videoId) {
    this.action = await Network.playVideo(
      videoId,
      parseInt(this.state.currentTime),
    );
  }

  updateVideo(time?: number) {
    Network.updateVideo(
      this.props.id,
      time || parseInt(this.state.currentTime || 0),
      this.action,
      this.state.end,
    );
  }

  togglePlayer() {
    const {id} = this.props;
    if (Platform.isTV) {
      if (this.state.paused) {
        this.playVideo(id);
        this.setState({end: 0});
        this.setUpdateInterval();
        this.setState({paused: false});
      } else {
        this.setState({end: 1});
        this.updateVideo();
        this.stopUpdateInterval();
        this.setState({paused: true});
      }
      return;
    }
    if (this.overlayOpacity._value == 1) {
      if (this.state.paused) {
        this.setState({end: 0});
        this.playVideo(id);
        this.setUpdateInterval();
        this.setState({paused: false});
      } else {
        this.setState({end: 1});
        this.updateVideo();
        this.stopUpdateInterval();
        this.setState({paused: true});
      }
    }
    this.playerOpacityTimer();
  }

  convertTime(time, maxTime) {
    let date = new Date(1000 * time);
    if (maxTime > 3600) {
      return date.toISOString().substr(11, 8);
    } else {
      return date.toISOString().substr(14, 5);
    }
  }

  getConvertedVideoDuration() {
    const {duration} = this.state;
    return this.convertTime(duration, duration);
  }

  getConvertedCurrentTime() {
    const {duration, currentTime} = this.state;
    return this.convertTime(currentTime, duration);
  }

  seekingTime(location) {
    this.playerOpacityTimer();
    this.setState({currentTime: location});
  }

  skipForward() {
    const newTime = this.state.currentTime + 5;

    if (Platform.isTV) {
      this.setState(
        {
          currentTime:
            newTime > this.state.duration ? this.state.duration : newTime,
        },
        () => {
          this.video.seek(this.state.currentTime);
        },
      );
    }
    this.playerOpacityTimer();
    if (this.skipForwardTimer) {
      this.setState(
        {
          currentTime:
            newTime > this.state.duration ? this.state.duration : newTime,
        },
        () => {
          this.video.seek(this.state.currentTime);
          this.skipForwardTimer = false;
        },
      );
    } else {
      this.skipForwardTimer = setTimeout(() => {
        this.skipForwardTimer = false;
      }, 300);
    }
  }

  skipBackward() {
    const newTime = this.state.currentTime - 5;

    if (Platform.isTV) {
      this.setState({currentTime: newTime >= 0 ? newTime : 0}, () => {
        this.video.seek(this.state.currentTime);
      });
      return;
    }
    this.playerOpacityTimer();
    if (this.skipBackwardTimer) {
      this.setState({currentTime: newTime >= 0 ? newTime : 0}, () => {
        this.video.seek(this.state.currentTime);
        this.skipBackwardTimer = false;
      });
    } else {
      this.skipBackwardTimer = setTimeout(() => {
        this.skipBackwardTimer = false;
      }, 300);
    }
  }

  renderVideoOverlay() {
    const {started, live, fullscreen, viewers} = this.props;
    const {
      portraitWidth,
      portraitHeight,
      landscapeWidth,
      landscapeHeight,
      landscapeMarginLeft,
      landscapeMarginRight,
      paused,
      currentTime,
      duration,
      loadingVideo,
      videoError,
      castModal,
      isTv,
    } = this.state;

    if (loadingVideo) {
      // while loading the video
      return (
        <View
          style={{
            height: fullscreen ? landscapeHeight : portraitHeight,
            width: fullscreen ? landscapeWidth : portraitWidth,
            marginLeft: fullscreen ? landscapeMarginLeft : 0,
            marginRight: fullscreen ? landscapeMarginRight : 0,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <PulseIndicator size={50} color={Colors.Indicator} />
        </View>
      );
    } else if (videoError) {
      return (
        <ImageBackground
          style={{
            width: fullscreen ? landscapeWidth : portraitWidth,
            height: fullscreen ? landscapeHeight : portraitHeight,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          source={{uri: this.context.activeCompany.default_video_image}}>
          <AppText style={{fontSize: 16}}>{global.strings.VideoError}</AppText>
        </ImageBackground>
      );
    } else if (started) {
      if (Platform.isTV) {
        return (
          <>
            <View
              style={{
                height: fullscreen ? landscapeHeight : portraitHeight,
                width: fullscreen ? landscapeWidth : portraitWidth,
                marginLeft: fullscreen ? landscapeMarginLeft : 0,
                marginRight: fullscreen ? landscapeMarginRight : 0,
                paddingTop: fullscreen ? 16 : 0,
              }}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Touchable
                  style={{marginLeft: 8, marginTop: 8}}
                  onPress={() => this.props.onBack()}>
                  <Icon
                    name="arrow-back"
                    size={30}
                    color={
                      this.context.activeCompany.font_color || Colors.Text1
                    }
                  />
                </Touchable>
              </View>
              {/* <View style={{ flex: 1 }} /> */}
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    width: '100%',
                    backgroundColor: 'rgba(0,0,0,.6)',
                    flexDirection: 'row',
                    height: 40,
                    alignItems: 'flex-end',
                  }}>
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: 'white',
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                      }}>
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <Touchable
                      style={{marginLeft: 8, marginTop: 8}}
                      onPress={() => this.skipBackward()}>
                      <Icon
                        name="fast-rewind"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                    <Touchable
                      style={{marginLeft: 8, marginTop: 8}}
                      onPress={() => this.togglePlayer()}>
                      <Icon
                        name={paused ? 'play-arrow' : 'pause'}
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                    <Touchable
                      style={{marginLeft: 8, marginTop: 8}}
                      onPress={() => this.skipForward()}>
                      <Icon
                        name="fast-forward"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                  </View>
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                        color: 'white',
                      }}>
                      {this.getConvertedVideoDuration()}
                    </AppText>
                  )}
                </View>
              </View>
            </View>
          </>
        );
      }
      return (
        <View>
          <Animated.View
            style={{
              height: fullscreen ? landscapeHeight : portraitHeight,
              width: fullscreen ? landscapeWidth : portraitWidth,
              marginLeft: fullscreen ? landscapeMarginLeft : 0,
              marginRight: fullscreen ? landscapeMarginRight : 0,
              paddingTop: fullscreen ? 16 : 0,
              opacity: this.overlayOpacity.interpolate({
                inputRange: [0, 1],
                outputRange: [0.0, 0.7],
              }),
            }}>
            <Touchable
              style={{flex: 1}}
              onPress={() => this.playerOpacityTimer()}>
              <LinearGradient
                colors={['rgba(0,0,0,.6)', 'transparent']}
                start={{x: 0.9, y: 1.0}}
                end={{x: 0.9, y: 0.0}}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                {/* overlay header -- LIVE tag if live and viewers counter */}
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 4,
                    height: 40,
                    justifyContent: live || isTv ? 'space-between' : 'flex-end',
                    alignSelf: 'stretch',
                  }}>
                  {live == 1 && (
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <View
                        style={{
                          backgroundColor: 'red',
                          height: 14,
                          width: 14,
                          borderRadius: 7,
                        }}
                      />
                      <AppText style={{fontSize: 14}}>
                        {' ' + global.strings.Live}
                      </AppText>
                    </View>
                  )}
                  {isTv && (
                    <Touchable
                      style={{marginLeft: 8, marginTop: 8}}
                      onPress={() => this.props.onBack()}>
                      <Icon
                        name="arrow-back"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                  )}
                  {viewers != 0 && viewers != null && (
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Icon
                        name="remove-red-eye"
                        size={14}
                        color={Colors.Text1}
                      />
                      <AppText style={{fontSize: 14, padding: 3}}>
                        {viewers}
                      </AppText>
                    </View>
                  )}
                </View>

                {/* overlay body -- play/pause button & seek(10) touchables on the sides */}
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Touchable
                    ref={ref => (this.tvButtons[1] = ref)}
                    style={{flex: 1}}
                    onPress={() => this.skipBackward()}
                  />
                  <Touchable
                    ref={ref => (this.playPauseButton = ref)}
                    onPress={() => this.togglePlayer()}
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name={paused ? 'play-arrow' : 'pause'}
                      size={40}
                      color={Colors.Text1}
                    />
                  </Touchable>
                  <Touchable
                    ref={ref => (this.tvButtons[3] = ref)}
                    style={{flex: 1}}
                    onPress={() => this.skipForward()}
                  />
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 8,
                  }}>
                  {/* Overlay footer */}
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: 'white',
                        opacity: 0.95,
                        fontSize: 14,
                        elevation: 2,
                      }}>
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  {/* Video seek slider */}
                  <View style={{flex: 1}}>
                    {!this.state.isTv && (
                      <Slider
                        value={currentTime}
                        onValueChange={value => this.seekingTime(value)}
                        onSlidingStart={value => this.setState({paused: true})}
                        onSlidingComplete={value => this.seek(value)}
                        style={{flex: 1}}
                        minimumValue={0}
                        maximumValue={live ? currentTime : duration}
                        minimumTrackTintColor={Colors.Text1}
                        maximumTrackTintColor="grey"
                      />
                    )}
                  </View>
                  {/* Video duration label */}
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: 'white',
                        opacity: 0.95,
                        fontSize: 14,
                        elevation: 2,
                      }}>
                      {this.getConvertedVideoDuration()}
                    </AppText>
                  )}
                  {live == 1 && (
                    <AppText
                      time
                      style={{
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                      }}>
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  {this.props.fullscreenController && !Platform.isTV && (
                    <Icon
                      name={fullscreen ? 'fullscreen-exit' : 'fullscreen'}
                      size={27}
                      color={Colors.Text1}
                      onPress={() => this.props.fullscreenController()}
                    />
                  )}
                </View>
              </LinearGradient>
            </Touchable>
          </Animated.View>
          <Modal
            visible={castModal}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.ModalBackground,
            }}>
            <View
              style={{
                marginRight: 16,
                marginLeft: 16,
                backgroundColor: Colors.ModalColor,
                elevation: 1,
                borderRadius: 8,
                width: '90%',
              }}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <AppText style={{fontSize: 16, padding: 16, paddingBottom: 0}}>
                  {global.strings.CastQuestion}
                </AppText>
              </View>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Touchable onPress={() => this.cast()}>
                  <AppText style={{fontSize: 16, padding: 8, marginBottom: 8}}>
                    {global.strings.Cast}
                  </AppText>
                </Touchable>
                <Touchable onPress={() => this.setState({castModal: false})}>
                  <AppText
                    style={{fontSize: 16, padding: 8, paddingBottom: 16}}>
                    {global.strings.Cancel}
                  </AppText>
                </Touchable>
              </View>
            </View>
          </Modal>
        </View>
      );
    } else {
      // view spacer in case the live is not started yet
      return <View style={{height: portraitHeight}} />;
    }
  }

  render() {
    const {uri, fullscreen, live, started} = this.props;
    const {
      portraitWidth,
      portraitHeight,
      landscapeWidth,
      landscapeHeight,
      landscapeMarginLeft,
      landscapeMarginRight,
      paused,
      currentTime,
    } = this.state;
    return (
      <View>
        {/* case: video or live already started */}
        {uri && started == 1 && (
          <Video
            style={{
              position: 'absolute',
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
              marginRight: fullscreen ? landscapeMarginRight : 0,
              marginLeft: fullscreen ? landscapeMarginLeft : 0,
            }}
            paused={paused}
            ref={(ref: Video) => {
              this.video = ref;
            }}
          
            onLoadStart={this.onLoadStart}
            onLoad={this.onLoad}
            onProgress={this.onProgress}
            onEnd={this.onEnd}
            onError={this.onError}
            source={{uri: uri}}
            muted={false}
            repeat={false}
            resizeMode={'contain'}
            volume={1}
            rate={1}
            ignoreSilentSwitch={"ignore"}
            // onProgressTime={thi.s}
          />
        )}

        {/* TODO: empty source -- is this a case? */}
        {uri === '' && (
          <Image
            source={{uri: this.context.activeCompany.default_video_image}}
            style={{
              position: 'absolute',
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
            }}
          />
        )}

        {/* case: Live not started */}
        {started == 0 && (
          <ImageBackground
            style={{
              position: 'absolute',
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <AppText style={{fontSize: 16, color: 'white'}}>
              <ActivityIndicator
                size="large"
                color={this.context.activeCompany.font_color}
              />
            </AppText>
          </ImageBackground>
        )}

        {/* render overlay */}
        {this.renderVideoOverlay()}
      </View>
    );
  }
}

export {VideoPlayer};
