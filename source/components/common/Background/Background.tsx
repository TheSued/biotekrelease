import React from 'react';
import { View, ImageBackground, SafeAreaView } from 'react-native';

import NetInfo from '@react-native-community/netinfo';

import { ActiveCompanyContext } from '../../../providers/CompanyProvider';
import Colors from '../../../environments/Colors';
import styles from './styles';
import BackgroundUI from './BackgroundUI';
import ScrollViewUI from './ScrollViewUI';

interface BackgroundProps {
  header?: any;
  style?: any;
  fullscreen?: boolean;
  type?: 'login' | 'simple' | 'live';
  login?: boolean;
  simple?: boolean;
  noscroll?: boolean;
  empty?: boolean;
}

interface BackgroundState {
  isConnected: boolean;
  backgroundColor: string;
  fontColor: string;
}

class Background extends React.PureComponent<BackgroundProps, BackgroundState> {
  static contextType = ActiveCompanyContext;

  constructor(props: BackgroundProps) {
    super(props);
    this.state = {
      isConnected: false,
      backgroundColor: Colors.Background2,
    };
  }

  componentDidMount() {
    NetInfo.fetch().then((state) => {
      this.setState({
        isConnected: state.isConnected,
        backgroundColor: this.getBackgroundColor(),
      });
    });
  }

  getBackgroundColor() {
    const { activeCompany } = this.context;
    if (this.props.fullscreen) {
      // player e streaming in fullscreen
      return 'black';
    } else if (activeCompany && activeCompany.background_color === '#fff') {
      activeCompany.background_color = Colors.Background3
      activeCompany.font_color = Colors.Text3
    }
    else {
      return (
        (activeCompany && activeCompany.background_color) || Colors.Background2 && Colors.Text1
      );
    }
  }

  getFontColor() {
    const { activeCompany } = this.context;
    if (activeCompany && activeCompany.font_color)
      return activeCompany.font_color
  }


  render() {
    const { backgroundColor } = this.state;
    if (this.props.empty) {
      return <View style={{ flex: 1, backgroundColor }} />;
    } else if (this.props.login && this.state.isConnected) {
      return (
        // login background
        <ImageBackground
          source={{ uri: this.context.activeCompany.image }}
          style={[
            styles.flex,
            this.props.style,
            { backgroundColor: Colors.Background2 },
          ]}>
          <ScrollViewUI style={this.props.style} header={this.props.header} fullscreen={this.props.fullscreen}>
            {this.props.children}
          </ScrollViewUI>
        </ImageBackground>
      );
    } else if (this.props.noscroll) {
      // used by StreamView
      return (
        <SafeAreaView
          style={[
            styles.flex,
            this.props.style,
            { backgroundColor: backgroundColor },
          ]}>
          {this.props.children}
        </SafeAreaView>
      );
    } else if (this.props.simple) {
      return (
        <BackgroundUI
          backgroundColor={backgroundColor}
          style={this.props.style}
          header={this.props.header}
          fullscreen={this.props.fullscreen}>
          {this.props.children}
        </BackgroundUI>
      );
    } else {
      return (
        <ScrollViewUI
          fullscreen={this.props.fullscreen}
          backgroundColor={backgroundColor}
          style={this.props.style}
          header={this.props.header}>
          {this.props.children}
        </ScrollViewUI>
      );
    }
  }
}

export { Background };
