import React from 'react';
import {StatusBar} from 'react-native';

const AppStatusBar = (props: any) => (
  <StatusBar hidden={props.fullscreen ? true : false} translucent={props.fullscreen ? false : true} backgroundColor={'transparent'} />
);

export default AppStatusBar;
