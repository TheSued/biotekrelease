import React from 'react';
import { View, ImageBackground, FlatList, PanResponder, Animated } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import { AppText, Touchable } from '../';
import { slideshowStyles } from './SlideshowStyles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../Background/styles';

class Slideshow extends React.PureComponent {
  private listRef: any;
  private currentItemIndex = 0;

  pan = new Animated.ValueXY({ x: 0, y: 0 });
  panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onPanResponderGrant: async () => {
      this.pan.setOffset({ x: this.pan.x._value, y: 0 });
    },
    onPanResponderMove: Animated.event([null, { dx: this.pan.x }]),
    onPanResponderEnd: () => {
      if (this.pan.x._value === 0) {
        this.props.onItemPress(this.props.data[this.currentItemIndex]);
      } else {
        this.scrollToIndex(this.pan.x._value < 0);
      }
      this.pan.setValue({ x: 0, y: 0 });
    },
    onPanResponderRelease: () => {
      this.pan.flattenOffset();
    },
  });

  private scrollToIndex(forward: boolean): void {
    if ((forward && this.currentItemIndex === (this.props.data.length) - 1) || (!forward && this.currentItemIndex === 0)) {
      return;
    }
    this.currentItemIndex = forward ? this.currentItemIndex + 1 : this.currentItemIndex - 1;
    this.listRef?.scrollToIndex({ index: this.currentItemIndex, viewPosition: 0.5 });
  }

  render() {
    const { data, title, onItemPress, onEndReached, defaultPreview } = this.props;

    if (data && data.length) {
      return (
        <>

          <AppText style={slideshowStyles.listTitleStyle}>{title}</AppText>
          {/* list */}
          <FlatList
            decelerationRate={'fast'}
            ref={(ref) => (this.listRef = ref)}
            showsHorizontalScrollIndicator={false}
            horizontal
            onPress={() => onItemPress(item)}
            data={data}
            scrollEnabled={false}
            renderItem={({ item, index }) => (
              <View>
                <ImageBackground
                  key={index}
                  resizeMode={'cover'}
                  source={{ uri: item.img_preview || defaultPreview }}
                  style={slideshowStyles.livePreviewStyle}
                  imageStyle={slideshowStyles.imageRadius}>
                  <Animated.View style={{ transform: [{ translateX: this.pan.x }, { translateY: this.pan.y }] }}
                    {...this.panResponder.panHandlers}>
                    <View style={{ height: 230, backgroundColor: 'transparent' }} />

                    <LinearGradient
                      colors={['black', 'transparent']}
                      start={{ x: 0.2, y: 1.0 }}
                      end={{ x: 0.2, y: 0.0 }}
                      style={slideshowStyles.translucentStyle}>
                      {item.duration ? (
                        <AppText style={slideshowStyles.durationStyle}>
                          {item.duration}
                        </AppText>
                      ) : (
                          <AppText />
                        )}
                    </LinearGradient>

                  </Animated.View>
                </ImageBackground>
                <Touchable onPress={() => onItemPress(item)}>
                  <AppText style={slideshowStyles.liveTitleStyle}>
                    {item.title + index} <Icon style={styles.iconSliderStyle} name="chevron-right-circle" size={16} />
                  </AppText>
                </Touchable>
              </View>
            )
            }
            keyExtractor={(item, index) => '' + item.id + index
            }
            style={slideshowStyles.listStyle}
            onEndReached={onEndReached}
          />

          {/*<View style={{ flex: 3 }}>
            <View style={slideshowStyles.dotContainerStyle}>
              {data.map((el, dotIndex) =>
                <View key={dotIndex} style={[slideshowStyles.dotStyle, this.index === dotIndex ? slideshowStyles.dotActive : slideshowStyles.dotInactive]} />
              )}
            </View>
              </View>*/}

        </>
      );
    } else {
      return null;
    }
  }
}

export { Slideshow };
