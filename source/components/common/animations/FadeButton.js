import React from 'react';
import { View, Animated } from 'react-native';

import { AppText, Touchable } from '../';

class FadeButton extends React.PureComponent {
  constructor(props) {
    super(props);
    this.alphaValue = new Animated.Value(0);
  }

  componentDidUpdate() {
    Animated.timing(this.alphaValue, {
      toValue: this.props.controller ? 1 : 0,
      duration: 500,
      useNativeDriver: false,
    }).start();
  }

  render() {
    return (
      <Animated.View
        style={{
          ...this.props.style,
          opacity: this.alphaValue.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.alphaValue[0], this.props.alphaValue[1]],
          }),
        }}>
        <Touchable style={[this.props.style, { flex: 1 }]} onPress={this.props.onPress}>
          <AppText style={this.props.textStyle}>{this.props.children}</AppText>
        </Touchable>
      </Animated.View>
    );
  }
}

export { FadeButton };
