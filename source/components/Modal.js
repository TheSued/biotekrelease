import React from 'react';

import { StyleSheet, View } from 'react-native';

import { ActiveCompanyContext } from '../providers/CompanyProvider';
import Colors from '../environments/Colors';

class Modal extends React.Component {
  static contextType = ActiveCompanyContext;

  getBackgroundColor() {
    const { activeCompany } = this.context;
    return (activeCompany && activeCompany.background_color) || Colors.Background2;
  }

  render() {
    if (this.props.visible) {
      return (
        <View style={[styles.modal, { backgroundColor: this.getBackgroundColor() }, this.props.style]}>
          {this.props.children}
          
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  modal: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
});

export { Modal };
