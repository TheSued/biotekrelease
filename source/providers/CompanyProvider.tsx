import React from 'react';

export const ActiveCompanyContext = React.createContext({});

export class ActiveCompanyProvider extends React.Component {
  state = {
    videoNotificationId: 0,
    isLoading: true,
    isSignedIn: false,
    user: null,
    paymentSuccessfull: false,
    activeCompany: {},
    companies: [],
    debugLog: [
      {
        id: '0',
        code: 'exampleCode1',
        message: 'exampleMessage1',
      },
      {
        id: '1',
        code: 'exampleCode2',
        message: 'exampleMessage2',
      },
      {
        id: '2',
        code: 'exampleCode3',
        message: 'exampleMessage3',
      },
    ],
  };

  render() {
    const {children} = this.props;
    return (
      <ActiveCompanyContext.Provider
        value={{
          ...this.state,
          setVideoNotificationId: (videoNotificationId: number) =>
            this.setState({videoNotificationId}),
          consumedVideoNotification: () =>
            this.setState({videoNotificationId: 0}),
          signedIn: (user: any) =>
            this.setState({isLoading: false, user}),
          signedOut: () => this.setState({isLoading: true, user: null},() => console.log('logout jndjhd')), // recalc user data to not write the code everywhere
          changeActiveCompany: (company: any) => {
            this.setState({activeCompany: company});
          },
          updateCompanyList: (companyList: any[], activeCompany: any) => {
            this.setState({
              companies: companyList,
              activeCompany: activeCompany || companyList[0],
            });
          },
          updateContent: (content: any[]) => this.setState({content}),
          togglePaymentSuccessfull: () =>
            this.setState({
              paymentSuccessfull: !this.state.paymentSuccessfull,
            }),
          addDebugLog: (log) =>
            this.setState({
              debugLog: [
                ...this.state.debugLog,
                {
                  id: '' + this.state.debugLog.length,
                  ...log,
                },
              ],
            }),
        }}>
        {children}
      </ActiveCompanyContext.Provider>
    );
  }
}

export const ActiveCompanyConsumer = ActiveCompanyContext.Consumer;
