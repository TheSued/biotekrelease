import React from 'react';

export const AuthContext = React.createContext({});

export class AuthProvider extends React.Component {
  state = {
    isLoading: false,
    isSignedIn: false,
    user: null,
  };

  render() {
    const {children} = this.props;
    return (
      <AuthContext.Provider
        value={{
          ...this.state,
          signedIn: (user: any) => this.setState({isLoading: false, isSignedIn: true, user}),
          signedOut: () => this.setState({isLoading: false, isSignedIn: false, user: null}),
        }}>
        {children}
      </AuthContext.Provider>
    );
  }
}
