import http from './http';
import Routes from '../environments/Routes';

export async function pay(data) {
  let paymentType;
  switch (data.type) {
    case 0:
      paymentType = 'f';
      break;
    case 1:
      paymentType = 'm';
      break;
    case 3:
      paymentType = 'q';
      break;
    case 6:
      paymentType = 'hy';
      break;
    case 12:
      paymentType = 'y';
      break;
    case 'giftcard':
      paymentType = data.type;
      break;
    default:
      console.log('Invalid payment type');
      break;
  }

  const formdata = new FormData();
  if (paymentType !== 'giftcard') {
    formdata.append('tokenStripe', data.clientToken);
    formdata.append('fullname', data.cardName);
    formdata.append('address', data.address);
    formdata.append('cap', data.cap);
    formdata.append('city', data.city);
    formdata.append('province', data.province);
    formdata.append('country', data.country);
  }
  formdata.append('id', data.id);
  formdata.append('type', paymentType);
  if (data.couponCode) {
    formdata.append('code', data.couponCode);
  }
  formdata.append('app', true);
  const response = (await http.post(Routes.Pay, formdata)) || [];
  if (response.length && response[0].status === 'success') {
    if (response[0].csec) {
      return response[0].csec;
    } else if (response[0].action_required === false) {
      return 1;
    } else {
      // status === 'success' for giftcard
      return 1;
    }
  }
}

export async function getPaymentInfo() {
  const paymentInfo = await http.get(Routes.GetPaymentInfo);
  return paymentInfo;
}

export async function checkCouponCode(code: string) {
  const formdata = new FormData();
  formdata.append('code', code);
  const response = (await http.post(Routes.ApplyCoupon, formdata)) || [];
  if (response[0] && response[0].status === 'success') {
    return response[0];
  }
  return null;
}
