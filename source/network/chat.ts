import http from './http';
import Routes from '../environments/Routes';
import { Alert } from 'react-native';

export async function getChat(id: number) {
  const formdata = new FormData();
  formdata.append('idl', id);
  const chat = (await http.post(Routes.ListChat, formdata)) || [];
  return chat;
}

// TODO: do we use this?
export async function qea(id: number) {
  const formdata = new FormData();
  formdata.append('idl', id);
  const partecipants =
    (await http.post(Routes.ListPartecipants, formdata)) || [];
  return partecipants;
}
export async function getQeA(id: number) {
  const formdata = new FormData();
  formdata.append('idl', id);
  const qea = (await http.post(Routes.ListQeA, formdata)) || [];
  return qea;
}

export async function getViewers(id: number) {
  const url = `${Routes.ViewersCounter}&id=${id}`;
  const viewers = await http.get(url);
  if (viewers) {
    return viewers.view_total;
  }
  return 0;
}

export async function sendMessage(
  id: number,
  time: number,
  user: string,
  message: string,
  avatar: string,
) {
  const formdata = new FormData();
  formdata.append('idl', id);
  formdata.append('time_live', time);
  formdata.append('user', user);
  formdata.append('message', message);
  formdata.append('avatar', avatar);
  await http.post(Routes.SendMessage, formdata);
}

export async function sendQeA(id: number, message: string) {
  const formdata = new FormData();
  formdata.append('idl', id);
  formdata.append('question', message);
  const send = await http.post(Routes.SendQeA, formdata);
  if (send.status == 'success') {
    return true;
  }
  else Alert.alert('hai superato il numero dei commenti, riprova tra poco');


}

export async function answerQuestion(id: number, message: string) {
  const formdata = new FormData();
  formdata.append('id', id);
  const response = (await http.post(Routes.AnswerQuestion, formdata)) || [];
  if (response[0].status === 'qea_add' && response[0].message === message) {
    return true;
  } else {
    return false;
  }
}

export async function getCommentQeA(id: number, step: number) {
  const url = `${Routes.ListQeA}&idl=${id}&step=${step}`;
  const message = await http.get(url);
  if (message) {
    return message;
  }
  return null;
}
