import { Platform, NativeModules } from "react-native";

import Strings_en_GB from "./Strings-en_GB";
import Strings_it_IT from "./Strings-it_IT";

export default function defineLanguage() {
  console.log();
  const locale = getLocale();
  if (locale.includes("it_")) {
    return Strings_it_IT;
  } else {
    return Strings_en_GB; // default - english
  }
}

function getLocale(): string {
  switch (Platform.OS) {
    case "ios":
      return (
        (NativeModules.SettingsManager.settings.AppleLocale &&
          NativeModules.SettingsManager.settings.AppleLocale.replace(
            "-",
            "_"
          )) ||
        NativeModules.SettingsManager.settings.AppleLanguages[0]
      );
    case "android":
      return NativeModules.I18nManager.localeIdentifier;
  }
  return "";
}
