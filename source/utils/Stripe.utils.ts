import { Platform } from 'react-native';
import stripe from 'tipsi-stripe';

export const setStripeEnv = (stripeId: string): void => {
  const stripeOptions: any = {
    //publishableKey: 'pk_live_BjV6A3yMDls2qZgtaYOoLtnB',
    publishableKey: 'pk_test_PukmX1zHYwrDuFKqVcGjAkCH',
  };
  Platform.select({
    ios: stripeOptions.merchantId = 'marcello.violini@teyuto.com',
    android: stripeOptions.androidPayMode = 'test',
  });
  stripe.setOptions(stripeOptions);
  stripe.setStripeAccount(stripeId);
};

// stripe test
// pk_test_PukmX1zHYwrDuFKqVcGjAkCH
// https://20200429t212837-dot-teyuto-live.ey.r.appspot.com/
// 4242 4242 4242 4242

// publishableKey: 'pk_live_BjV6A3yMDls2qZgtaYOoLtnB',
// publishableKey: 'pk_test_PukmX1zHYwrDuFKqVcGjAkCH',
