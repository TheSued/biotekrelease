import React from 'react';
import { View, FlatList, Image, BackHandler } from 'react-native';

import { activateKeepAwake, deactivateKeepAwake } from '@sayem314/react-native-keep-awake';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NodeCameraView } from 'react-native-nodemediaclient';
import Orientation from 'react-native-orientation-locker';
import Pusher from 'pusher-js/react-native';

import { ActiveCompanyContext } from '../providers/CompanyProvider';
import * as Network from '../network';
import Colors from '../environments/Colors';

import { Modal, Toast } from '../components';
import { Background, AppText, Touchable } from '../components/common';

const currentScreen = 'StreamView';

export default class StreamView extends React.Component {
  static contextType = ActiveCompanyContext;

  state = {
    streamId: undefined,
    streaming: false,
    permissions: true,
    chat: [],
    uri: undefined,
    viewers: 0,
    timeCounter: 0,
    chatColor: 'rgba(0,0,0,0.65)',
    selectedQuestion: {},
    errorMessage: '',
    rightOrientation: false,
    ignoreOrientation: false,

    pageHasFocus: false,
  };

  viewWillFocus() {
    this.setState({ pageHasFocus: true });

    activateKeepAwake();
    Orientation.lockToLandscapeLeft();
    Orientation.addDeviceOrientationListener(this._orientationDidChange);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => this.goBack());

    if (this.props.route.params.streamId !== this.state.streamId) {
      // reset params
      const { streamId, streamData } = this.props.route.params;
      this.setState({
        streamId: streamId,
        uri: 'rtmp://' + streamData.serverLive + ':1935/live/' + streamData.streamKey + '?c=' + streamData.streamCode,
        chat: [],
        timeCounter: 0,
      });
      if (this.chatChannel && this.socket && this.viewerChannel) {
        this.chatChannel.unbind_global();
        this.viewerChannel.unbind_global();
        this.socket.unsubscribe();
      }
      this.chatChannel = this.socket.subscribe('teyuto_live_chat_' + streamId);
      this.chatChannel.bind('live_chat', (data) => {
        const dataParse = JSON.parse(data);
        this.setState({ chat: [...this.state.chat, ...dataParse] }, () => setTimeout(() => this.chat.scrollToEnd(), 300));
      });
      this.viewerChannel = this.socket.subscribe('live_' + streamId);
      this.viewerChannel.bind('notification', (data) => {
        const dataParse = JSON.parse(data);
        if (dataParse.type === 'currently_users') {
          this.setState({ viewers: dataParse.val });
        }
      });
    } else {
      this.viewerChannel = this.socket.subscribe('live_' + this.state.streamId);
      this.viewerChannel.bind('notification', (data) => {
        const dataParse = JSON.parse(data);
        if (dataParse.type === 'currently_users') {
          this.setState({ viewers: dataParse.val });
        }
      });
      this.chatChannel = this.socket.subscribe('teyuto_live_chat_' + this.state.streamId);
      this.chatChannel.bind('live_chat', (data) => {
        const dataParse = JSON.parse(data);
        this.setState({ chat: [...this.state.chat, ...dataParse] }, () => setTimeout(() => this.chat.scrollToEnd(), 300));
      });
    }
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });

    deactivateKeepAwake();
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.chatChannel.unbind_global();
    this.socket.unsubscribe();
    if (this.state.focus) {
      this.setState({ focus: false, streaming: false });
    }
    if (this.stream) {
      this.stream.stop();
    }
    Orientation.unlockAllOrientations();
  }

  componentWillUnmount() {
    if (this.chatChannel) {
      this.chatChannel.unbind_global();
    }
    if (this.socket) {
      this.socket.unsubscribe();
    }
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusListener();
    this.blurListener();
  }

  componentWillMount() {
    this.socket = new Pusher('1951c674aef8e8b86430', {
      cluster: 'eu',
    });
    this.focusListener = this.props.navigation.addListener('focus', () => this.viewWillFocus());
    this.blurListener = this.props.navigation.addListener('blur', () => this.viewWillBlur());
    this.cameraHeight = global.screenProp.width;
    this.cameraWidth = (global.screenProp.width / 9) * 16;
    this.cameraHorizontalMargin = (global.screenProp.height - (global.screenProp.width / 9) * 16) / 2;
  }

  async goBack() {
    const { previousScreen } = this.props.route.params;
    this.viewWillBlur();
    this.props.navigation.goBack();
  }

  _orientationDidChange = (orientation) => {
    if (orientation == 'LANDSCAPE-LEFT') {
      this.setState({ rightOrientation: true });
    } else {
      this.setState({ rightOrientation: false });
    }
  };

  toggleStream() {
    if (this.state.streaming) {
      clearInterval(this.timeCounterTimer);
      this.stream.stop();
      this.setState({ streaming: false });
    } else {
      this.timeCounterTimer = setInterval(() => this.setState({ timeCounter: this.state.timeCounter + 1 }), 1000);
      this.stream.start();
      this.setState({ streaming: true });
    }
  }

  convertTime() {
    var date = new Date(1000 * this.state.timeCounter);
    if (this.state.timeCounter > 3600) {
      return date.toISOString().substr(11, 8);
    } else {
      return date.toISOString().substr(14, 5);
    }
  }

  async answerQuestion() {
    const { selectedQuestion } = this.state;
    const response = await Network.answerQuestion(selectedQuestion.idChat, selectedQuestion.message);
    if (response) {
      this.setState({ errorMessage: global.strings.AnswerSuccessfull }, () =>
        setTimeout(() => this.setState({ errorMessage: '' }), 3000),
      );
      this.setState({ selectedQuestion: {} });
    } else {
      this.setState({ errorMessage: global.strings.GeneralErrorShort }, () =>
        setTimeout(() => this.setState({ errorMessage: '' }), 3000),
      );
      this.setState({ selectedQuestion: {} });
    }
  }

  logStatus(status) {
    this.context.addDebugLog(status);
  }

  render() {
    if (this.state.pageHasFocus) {
      const {
        permissions,
        uri,
        viewers,
        streaming,
        chatColor,
        selectedQuestion,
        rightOrientation,
        ignoreOrientation,
      } = this.state;

      if (permissions) {
        return (
          <Background fullscreen={true} noscroll>
            {/* camera preview*/}
            <View
              style={{
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                backgroundColor: 'black',
              }}>
              <NodeCameraView
                style={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  right: this.cameraHorizontalMargin,
                  left: this.cameraHorizontalMargin,
                }}
                ref={(stream) => (this.stream = stream)}
                // onStatus={(code, message) => this.logStatus({code, message})}
                outputUrl={uri}
                camera={{ cameraId: 1, cameraFrontMirror: true }}
                audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
                video={{
                  preset: 4,
                  // bitrate: 1250 * 1024,
                  bitrate: (this.context.activeCompany.bitrate || 2500) * 1024,
                  // profile: 2,
                  profile: 0,
                  fps: 30,
                  videoFrontMirror: false,
                }}
                autopreview={true}
              />
            </View>

            {/* overlay */}
            <View
              style={{
                flex: 1,
                backgroundColor: Colors.PlaceholderText,
                marginHorizontal: this.cameraHorizontalMargin,
              }}>
              {/* top */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 20,
                }}>
                {streaming && (
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View
                      style={{
                        height: 16,
                        width: 16,
                        borderRadius: 8,
                        backgroundColor: 'red',
                        marginLeft: 8,
                      }}
                    />
                    <AppText style={{ fontSize: 18, marginLeft: 4 }}>
                      {' ' + global.strings.LIVE + ' '}
                    </AppText>
                    <AppText style={{ fontSize: 18, marginLeft: 4 }}>
                      {' ' + this.convertTime() + ' '}
                    </AppText>
                  </View>
                )}
                {!streaming && (
                  <Icon name="arrow-back" size={22} color={'rgba(255,255,255,0.8)'} onPress={() => this.goBack()} />
                )}
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 8,
                    justifyContent: 'center',
                  }}>
                  <Icon name="remove-red-eye" size={22} color={'rgba(255,255,255,0.8)'} />
                  {this.context.activeCompany.show_video_views && <AppText style={{ fontSize: 18, color: 'rgba(255,255,255,0.8)' }}>{' ' + viewers + ' '}</AppText>}
                </View>
              </View>

              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ justifyContent: 'flex-end' }}>
                  <Icon
                    name="switch-camera"
                    size={30}
                    color={'rgba(255,255,255,0.8)'}
                    style={{ padding: 16 }}
                    onPress={() => this.stream.switchCamera()}
                  />
                </View>
                <View
                  style={{
                    flex: 2,
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}>
                  {/* toggle stream button */}
                  <Touchable style={{
                    height: 50,
                    width: 50,
                    backgroundColor: streaming ? 'grey' : 'red',
                    borderRadius: 25,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 16,
                    opacity: 0.8,
                  }} onPress={() => this.toggleStream()}>

                    <View
                      style={{
                        height: 20,
                        width: 20,
                        backgroundColor: 'white',
                        borderRadius: streaming ? 0 : 10,
                      }}
                    />
                  </Touchable>
                </View>
                {/* chat */}
                {this.context.activeCompany.video_chat && <View style={{ flex: 1 }}>
                  <FlatList
                    ref={(ref) => {
                      this.chat = ref;
                    }}
                    ListFooterComponent={<View style={{ height: 8 }} />}
                    ListEmptyComponent={<View style={{ flex: 1 }} />}
                    showsVerticalScrollIndicator={true}
                    data={this.state.chat}
                    ItemSeparatorComponent={() => <View style={{ height: 8 }} />}
                    renderItem={({ item }) => (
                      <Touchable style={{
                        alignSelf: 'stretch',
                        backgroundColor: chatColor,
                        padding: 8,
                        borderRadius: 5,
                        marginRight: 25,
                      }} onPress={() => this.setState({ selectedQuestion: item })}>

                        <View style={{ flexDirection: 'row' }}>
                          <Image source={{ uri: item.avatar }} style={{ height: 28, width: 28, borderRadius: 14 }} />
                          <AppText
                            style={{
                              fontWeight: '600',
                              fontSize: 18,
                              color: item.nameColor || Colors.Text1,
                            }}>
                            {item.user}
                          </AppText>
                        </View>
                        <AppText
                          style={{
                            marginLeft: 8,
                            marginBottom: 12,
                            fontSize: 16,
                          }}
                          multiline
                          autogrow
                          maxHeight={130}>
                          {item.message}
                        </AppText>
                      </Touchable>
                    )}
                    keyExtractor={(item) => item.idChat}
                    style={{ flex: 1 }}
                  />
                </View>}
              </View>
            </View>
            <Modal
              visible={selectedQuestion.idChat || (!rightOrientation && !ignoreOrientation)}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, .03)',
              }}>
              {selectedQuestion.idChat && (
                <View style={{ backgroundColor: Colors.ModalColor, borderRadius: 8 }}>
                  <AppText style={{ fontSize: 16, padding: 4, color: Colors.Text1 }}>
                    {global.strings.AnswerQuestion1 + selectedQuestion.user + global.strings.AnswerQuestion2}
                  </AppText>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 4,
                      alignItems: 'space-between',
                    }}>
                    <Touchable style={{
                      flex: 1,
                      alignItems: 'center',
                      borderWidth: 1,
                      padding: 4,
                      borderBottomLeftRadius: 8,
                    }} onPress={() => this.answerQuestion()}>

                      <AppText style={{ fontSize: 16, color: Colors.Text1 }}>{global.strings.Answer}</AppText>

                    </Touchable>
                    <Touchable style={{
                      flex: 1,
                      alignItems: 'center',
                      borderWidth: 1,
                      padding: 4,
                      borderBottomRightRadius: 8,
                    }} onPress={() => this.setState({ selectedQuestion: {} })}>

                      <AppText style={{ fontSize: 16, color: Colors.PlaceholderText }}>{global.strings.Cancel}</AppText>
                    </Touchable>
                  </View>
                </View>
              )}
              {!rightOrientation && !ignoreOrientation && (
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'black',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '100%',
                  }}>
                  <Icon name="screen-rotation" size={55} color={Colors.Text1} />
                  <Touchable style={{
                    backgroundColor: '#383838',
                    padding: 12,
                    borderRadius: 15,
                    marginTop: 8,
                  }} onPress={() => this.setState({ ignoreOrientation: true })}>

                    <AppText style={{ color: Colors.Text1, fontSize: 20 }}>{global.strings.Ignore}</AppText>

                  </Touchable>
                </View>
              )}
            </Modal>
            <Toast message={this.state.errorMessage} />
          </Background >
        );
      } else {
        return (
          <Background>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <AppText style={{ fontSize: 16 }}>{global.strings.StreamPermissions}</AppText>
            </View>
          </Background>
        );
      }
    } else {
      return <Background empty />;
    }
  }
}

export { StreamView };
