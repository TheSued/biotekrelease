import React from "react";
import { View, Image, StyleSheet, Keyboard } from "react-native";

import { ActiveCompanyContext } from "../providers";
import { LoginButton } from "../components";
import {
  Background,
  AppText,
  Input,
  Expand,
  Touchable,
} from "../components/common";

import Colors from "../environments/Colors";

import Storage from "../services/Storage";
import * as Network from "../network";
import { isCustomApp } from "../environments/AppEnvironment";

interface LoginProps {
  navigation: any;
}

class LoginForm extends React.PureComponent<LoginProps> {
  static contextType = ActiveCompanyContext;

  state = {
    email: "",
    password: "",
    isLoading: true,
    isLogging: false,
    showError: false,
    message: "",
    anim: false,
    pageHasFocus: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.setState({ pageHasFocus: true })
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.setState({ pageHasFocus: false })
    );
  }

  navigateToRegistration() {
    this.props.navigation.navigate("Registration");
  }

  navigateToHomepage() {
    this.props.navigation.navigate("Homepage");
  }

  onError(message: string) {
    this.setState({ message: message }, () =>
      setTimeout(() => this.setState({ showError: true }), 500)
    );
  }

  async onSubmit() {
    Keyboard.dismiss();

    if (this.state.email !== "" && this.state.password !== "") {
      this.setState({ isLogging: true, message: "", showError: false });
      setTimeout(async () => {
        const token = await Network.login(
          this.state.email,
          this.state.password
        );
        if (token !== -1 && token !== -2) {
          await Storage.saveToken(token);
          this.context.signedOut();
        } else if (token === -1) {
          this.setState({ isLogging: false }, () =>
            this.onError(global.strings.CredentialsError)
          );
        } else {
          this.setState({ isLogging: false }, () =>
            this.onError(global.strings.NetworkError)
          );
        }
      }, 500);
    } else {
      this.onError(global.strings.NoCredentials);
    }
  }

  renderView() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Image
          resizeMode={"contain"}
          source={require("../../assets/images/logo_mini.png")}
          style={styles.imageStyle}
        />

        <Expand
          params={{ start: 0, end: 30 }}
          controller={this.state.message == "" ? false : true}
          style={{ marginTop: 13 }}
        >
          <AppText style={styles.errorTextStyle}>
            {" "}
            {this.state.showError ? this.state.message : ""}{" "}
          </AppText>
        </Expand>

        <View style={styles.formContainer1}>
          <Input
            autoCapitalize={"none"}
            autoCorrect={false}
            onChangeText={(text: string) => this.setState({ email: text })}
            value={this.state.email}
            placeholder={global.strings.EmailPlaceholder}
            style={styles.formInput}
          />
        </View>

        <View style={styles.formContainer2}>
          <Input
            onChangeText={(text: string) => this.setState({ password: text })}
            value={this.state.password}
            placeholder={global.strings.PasswordPlaceholder}
            secureTextEntry
            style={styles.formInput}
          />
        </View>

        <LoginButton
          style={{
            borderColor: "rgb(129, 129, 130)",
          }}
          controller={this.state.isLogging}
          onSubmit={() => this.onSubmit()}
        />

        {this.context.activeCompany.registration_allowed && (
          <View style={{ alignItems: "center" }}>
            <AppText style={{ marginTop: 30, color: "rgb(129, 129, 130)" }}>
              {global.strings.NoAccount}
            </AppText>
            <Touchable onPress={() => this.navigateToRegistration()}>
              <AppText
                style={{
                  marginTop: 12,
                  fontWeight: "bold",
                  fontSize: 16,
                  textDecorationLine: "underline",
                  color: "white",
                }}
              >
                {global.strings.SignUp}
              </AppText>
            </Touchable>
          </View>
        )}
        {this.context.activeCompany.home_guest && (
          <Touchable
            style={{ alignItems: "center", marginTop: 16, color: "white" }}
            onPress={() => this.navigateToHomepage()}
          >
            <AppText
              style={{
                marginVertical: 12,
                fontSize: 16,
              }}
            >
              {global.strings.Skip}
            </AppText>
          </Touchable>
        )}
        <View style={styles.footerContainer}>
          <AppText style={styles.footerText}>
            {global.strings.footerText}
            <AppText style={styles.footerCompanyTitle}>
              {global.strings.footerCompanyTitle}
            </AppText>
          </AppText>
        </View>
      </View>
    );
  }

  render() {
    if (this.state.pageHasFocus) {
      return (
        <Background login style={styles.backgroundStyle}>
          {this.renderView()}
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },

  // logo
  backgroundStyle: {
    justifyContent: "space-around",
    alignItems: "center",
  },
  imageStyle: {
    height: 107,
    width: 190,
    alignSelf: "center",
    marginBottom: 24,
  },

  // error text
  errorTextStyle: {
    alignSelf: "center",
    color: Colors.TextError,
    fontSize: 17,
    elevation: 1,
  },

  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
    borderWidth: 1,
    borderStyle: "solid",
    elevation: 1,
  },
  buttonText: {
    fontSize: 22,
    paddingTop: 12,
    paddingBottom: 12,
    elevation: 1,
  },
  buttonTextActive: {
    color: Colors.Text1,
  },
  buttonTextInactive: {
    color: Colors.PlaceholderText,
  },
  buttonBorderActive: {
    borderColor: Colors.Text1,
  },
  buttonBorderInactive: {
    borderColor: Colors.PlaceholderText,
  },

  formContainer1: {
    width: 300,
    backgroundColor: Colors.Background1,
    marginTop: 13,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  formContainer2: {
    width: 300,
    backgroundColor: Colors.Background1,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    marginTop: 2,
  },
  formInput: {
    fontSize: 20,
    color: Colors.Text1,
    height: 60,
    paddingLeft: 15,
  },
  logoStyle: {
    height: 134,
    width: 200,
  },
  footerContainer: {
    width: "80%",
    textAlign: "center",
    marginTop: 100,
    color: "white",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  footerText: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontSize: 14,
  },
  footerCompanyTitle: {
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontSize: 17,
  },
});

export { LoginForm };
