import React from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';
import { LogoutButton, Modal } from '../components';
import Colors from '../environments/Colors';
import Storage from '../services/Storage';
import { getInitialNotifications } from '../utils/Notifications.utils';
import { AppCompanyId, isCustomApp } from '../environments/AppEnvironment';
import * as Network from '../network';
import { ActiveCompanyContext } from '../providers';
import AppStatusBar from '../components/common/AppStatusBar/AppStatusBar';
import { AppText, Touchable } from '../components/common';

class Splashscreen extends React.PureComponent {
  static contextType = ActiveCompanyContext;
  state = {
    verifyMailStep: false,
  }

  componentDidMount() {
    this.initializeApp();
  }

  async initializeApp() {
    await this.setCompany();
    const token = await Storage.getToken();
    console.log('tokenverifica', token);
    if (token) {
      const notification = await getInitialNotifications();
      await this.initializeUser(notification);
      return;
    } else {
      this.context.signedIn(null);
    }
  }

  async initializeUser(notification: number | null) {
    const data = await this.getData();

    if (data) {
      const userData = this.formatUser(data);
      const companySettings = await Network.getCompanySettings(data.companies[0].id, Platform.OS);
      const companyData = this.formatCompany(data, companySettings);
      if (!data.email_verified) {
        this.setState({ verifyMailStep: true });
        return;
      }
      if (notification) {
        this.context.setVideoNotificationId(notification);
      }
      this.context.signedIn(userData);
      return;
    } else {
      this.context.signedIn(null);
    }
  }

  async setCompany() {
    const companyData = await Network.getCompanySettings(
      AppCompanyId,
      Platform.OS
    );
    this.context.changeActiveCompany({
      id: AppCompanyId,
      ...JSON.parse(JSON.stringify(companyData)),
    });
  }

  async getData() {
    const companyId = isCustomApp() && AppCompanyId;
    const data = await Network.getUserData(companyId);
    return data;
  }

  formatUser(data) {
    return {
      displayName: data.display_name,
      profileImage: data.profile_image,
      role: data.role,
      work: data.work,
      followers: data.followers,
      live: data.live,
      email: data.email,
      sid: data.sid,
      creator: data.companies[0].creator,
    };
  }

  formatCompany(data, activeCompanySettings) {
    const { logo, default_video_image } = this.context.activeCompany;
    data.companies.forEach((c) => {
      c.logo = c.logo || logo;
      c.default_video_image = c.default_video_image || default_video_image;
      c.image = c.image || default_video_image;
    });
    const activeCompany = data.companies.find(
      (c: any) => c.id === data.id_company
    );
    return {
      companies: data.companies,
      activeCompany: {
        ...activeCompany,
        ...activeCompanySettings,
        id: data.id_company,
      },
    };
  }

  render() {

    return (
      <>
        <AppStatusBar />
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'black',
            flexDirection: 'column',
          }}
        >
          {!this.state.verifyMailStep &&
            <Touchable onPress={() => console.log('')}>
              <Image
                resizeMode={'contain'}
                source={require('../../assets/images/logo_mini.png')}
                style={styles.logoStyle}
              />
            </Touchable>
          }
          {this.state.verifyMailStep &&
            <>
              <Touchable onPress={() => console.log('')}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/logo_mini.png')}
                  style={styles.logoEmailStyle}
                />
              </Touchable>
              <AppText style={styles.emailTitle}>{global.strings.VerifyEmailError}</AppText>
              <AppText style={styles.emailSubTitle}>{global.strings.VerifyEmail}</AppText>
              <Touchable style={{ padding: 15, backgroundColor: 'rgba(255,255,255,2)', marginTop: 20 }} onPress={() => this.setState({ verifyMailStep: false }, () => this.initializeUser(null))}>
                <AppText style={styles.buttonText}>{global.strings.TryAgain}</AppText>
              </Touchable>



              <Touchable onPress={async () => { await Storage.deleteToken(); this.context.signedOut(); this.setState({ verifyMailStep: false }, () => this.initializeApp()) }}
                style={{ position: 'absolute', bottom: 0, justifyContent: 'center', padding: 5, marginBottom: 80, borderBottomColor: 'white', borderBottomWidth: 2, }}>
                <AppText style={styles.buttonTextLogout}>Logout</AppText>
              </Touchable>
            </>
          }
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  logoStyle: {
    height: 134,
    width: 200,
  },

  logoEmailStyle: {
    height: 100,
    width: 100,
    marginBottom: 50,
  },

  emailTitle: {
    fontWeight: '700',
    fontSize: 25,
    marginBottom: 15,

  },

  emailSubTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 20,
    textAlign: 'center',
  },

  buttonText: {
    fontWeight: '700',
    fontSize: 19,
    paddingHorizontal: 15,

  },
  buttonTextLogout: {
    fontWeight: '700',
    fontSize: 19,
    paddingHorizontal: 15,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  }


});

export { Splashscreen };
