import React from "react";
import { View, Image, StyleSheet, BackHandler } from "react-native";

import LinearGradient from "react-native-linear-gradient";

import { Header, LogoutButton, Modal } from "../components";
import ContactsButton from "../components/ContactsButton";
import CandidateButton from "../components/CandidateButton";
import { Background, AppText, Touchable } from "../components/common";
import Routes from "../environments/Routes";
import Colors from "../environments/Colors";
import { ActiveCompanyContext } from "../providers";
import Storage from "../services/Storage";
import * as Network from "../network";

const currentScreen = "Profile";
class Profile extends React.Component {
  static contextType = ActiveCompanyContext;
  state = {
    logoutModal: false,
    pageHasFocus: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.viewWillFocus()
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.viewWillBlur()
    );
  }

  viewWillFocus() {
    this.setState({ pageHasFocus: true });

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () =>
      this.goBack()
    );
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });
    this.setState({ logoutModal: false });
    if (this.backHandler) {
      this.backHandler.remove();
    }
  }

  componentWillUnmount() {
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusListener();
    this.blurListener();
  }

  async goBack() {
    this.props.navigation.goBack();
    if (!data.email_verified) {
      this.setState({ verifyMailStep: true });
      return;
    }
  }

  render() {
    if (this.state.pageHasFocus) {
      return (
        <Background
          header={
            <Header
              onCameraPress={() =>
                this.props.navigation.navigate("ManageLive", {
                  previousScreen: currentScreen,
                })
              }
              onMenuPress={() => this.goBack()}
              onLogoPress={() => this.goBack()}
            />
          }
        >
          {/* user image */}
          {!!this.context.user.profileImage && (
            <View style={styles.gradientContainerStyle}>
              <LinearGradient
                colors={[Colors.Secondary1, Colors.Secondary2]}
                start={{ x: 0.0, y: 0.5 }}
                end={{ x: 1.0, y: 0.5 }}
                style={styles.profileImageGradientStyle}
              >
                <View style={styles.profileImageContainerStyle}>
                  <Image
                    resizeMode="cover"
                    source={{
                      uri: this.context.user.profileImage,
                    }}
                    style={{ width: 120, height: 120, borderRadius: 60 }}
                  />
                </View>
              </LinearGradient>
            </View>
          )}
          <AppText style={styles.nameStyle}>
            {" "}
            {this.context.user.displayName}{" "}
          </AppText>

          <View
            style={{ flex: 4, alignItems: "center", justifyContent: "center" }}
          >
            <LogoutButton
              controller={this.state.logoutModal}
              onSubmit={() => this.setState({ logoutModal: true })}
            />
          </View>
          <View
            style={{ flex: 2, alignItems: "center", justifyContent: "center" }}
          >
            <ContactsButton />
            <CandidateButton />
          </View>

          {/* <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('DebuggerView')}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <AppText style={{fontSize: 16, padding: 8, paddingBottom: 16, color: Colors.PlaceholderText}}>
              {'Debugger'}
            </AppText>
          </View>
        </TouchableWithoutFeedback> */}
          {/* Logout Modal */}
          <Modal
            style={{
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.ModalColor,
            }}
            visible={this.state.logoutModal}
          >
            <View
              style={{
                borderRadius: 8,
                width: "90%",
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <AppText
                  style={{
                    fontSize: 16,
                    padding: 16,
                    paddingBottom: 0,
                    color: Colors.Text1,
                  }}
                >
                  {global.strings.LogoutQuestion}
                </AppText>
              </View>
              <View
                style={{
                  display: "flex",
                  marginTop: 4,
                  flexDirection: "row",
                }}
              >
                <Touchable
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onPress={async () => {
                    await Storage.deleteToken();
                    this.context.signedOut();
                    () => this.initializeApp();
                  }}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      padding: 8,
                      paddingBottom: 16,
                      color: Colors.Text1,
                    }}
                  >
                    {global.strings.Yes}
                  </AppText>
                </Touchable>
                <Touchable
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onPress={() => this.setState({ logoutModal: false })}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      padding: 8,
                      paddingBottom: 16,
                      color: Colors.PlaceholderText,
                    }}
                  >
                    {global.strings.Cancel}
                  </AppText>
                </Touchable>
              </View>
            </View>
          </Modal>
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  whiteColor: {
    color: Colors.Text1,
  },

  // user image
  gradientContainerStyle: {
    alignItems: "center",
    paddingTop: 28,
  },
  profileImageGradientStyle: {
    borderRadius: 80,
    height: 136,
    width: 136,
    elevation: 0.5,
  },
  profileImageContainerStyle: {
    width: 120,
    height: 120,
    borderRadius: 60,
    // backgroundColor: Colors.Text1,
    top: 8,
    elevation: 1,
    left: 8,
  },

  // user data
  userDataContainerStyle: {
    flexDirection: "row",
    height: 180,
  },
  counterContainerStyle: {
    flex: 2,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  counterTextStyle: {
    alignSelf: "center",
    fontSize: 22,
    fontWeight: "500",
    elevation: 1,
  },
  nameStyle: {
    alignSelf: "center",
    fontWeight: "bold",
    fontSize: 32,
    marginTop: 12,
  },
  occupationContainerStyle: {
    flexDirection: "row",
    justifyContent: "center",
  },
  companyTextStyle: {
    fontSize: 17,
    color: Colors.Text2,
    marginTop: 2,
  },
  jobTextStyle: {
    marginTop: 2,
    fontSize: 17,
    color: Colors.Text2,
  },

  // footer
  footerStyle: {
    position: "absolute",
    height: 55,
    bottom: 30,
    right: 0,
    left: 0,
  },
  buttonIconContainerStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonGradientStyle: {
    position: "absolute",
    height: 80,
    width: 80,
    alignSelf: "center",
    borderRadius: 50,
    elevation: 1,
  },
  buttonIconStyle: {
    color: Colors.Text1,
    fontWeight: "600",
    alignSelf: "center",
  },
  buttonStyle: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },

  // modal
  modalWindowContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  modalWindowStyle: {
    backgroundColor: Colors.Text1,

    borderRadius: 7,
    elevation: 1,
  },
  modalQuestionContainerStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  modalQuestionStyle: {
    fontSize: 18,
  },
  modalAnswerContainerStyle: {
    alignSelf: "flex-end",
    flexDirection: "row",
  },
  modalAnswerTextStyle: {
    borderColor: Colors.Border1,
    width: 150,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
  },
});

export { Profile };
