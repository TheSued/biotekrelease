import React from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  Animated,
  FlatList,
  BackHandler,
  TouchableOpacity,
  StatusBar,
  Platform,
  ScrollView,
  Text,
  Image,
  Linking,
  Alert,
} from 'react-native';

import {Picker} from '@react-native-community/picker';
import Storage from '../services/Storage';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconPlay from 'react-native-vector-icons/FontAwesome';
import {PulseIndicator} from 'react-native-indicators';
import LinearGradient from 'react-native-linear-gradient';
import Orientation from 'react-native-orientation-locker';

import * as Network from '../network';
import Colors from '../environments/Colors';

import {Background, AppText, Touchable} from '../components/common';
import {Header, Modal, HomePageList} from '../components';
import {ActiveCompanyContext} from '../providers/CompanyProvider';
import {formatVideoObject} from '../utils/common.utils';

const currentScreen = 'SerieView';

class SerieView extends React.Component {
  static contextType = ActiveCompanyContext;
  backHandler: any;
  overlayOpacity: Animated.Value;

  state = {
    access: false, // if the user can view this serie

    // serie
    currentSerie: {
      id: null,
      data: [],
      step: 1,
      loading: false,
    },
    // trailer: false,
    errorMessage: '',
    packSeries: '',

    // switch between watch trailer and normal SerieView
    fullscreen: false,

    // trailer states
    screenHeight: (global.screenProp.width / 16) * 9,
    screenWidth: global.screenProp.width - 6,
    trailerSource: undefined,
    muted: false,
    paused: true,
    duration: 0,
    currentTime: 0,

    bundleInfo: [],

    // these control the render of the pay buttons
    paySerie: false,
    paySubscription: false,
    subscription: {
      period: 0,
      price: 0,
    },
    subscriptionPrice: 0,
    subscriptionPeriod: 0,

    pageHasFocus: false,
    elementPreviewMarker: [],
    elementShowPreview: [],
  };

  // TODO: misleading fullscreen or trailer?

  video: Video;

  onBuffer = data => {
    this.setState({duration: data.duration});
  };

  onLoad = data => {
    this.setState({duration: data.duration ? data.duration : 0});
  };

  onProgress = data => {
    // console.log("PLAY SERIES", this.state.currentTime)
    this.setState({currentTime: data.currentTime});
  };

  onEnd = () => {
    this.video.seek(0);
    this.setState({paused: true, trailer: false}, () =>
      Orientation.lockToPortrait(),
    );
  };

  async getTrailer(trailer) {
    const videoInfo = await Network.getVideoSource(trailer);
    if (videoInfo) {
      let video = formatVideoObject(videoInfo);
      this.setState({
        trailerSource: video.fullhd || video.hd || video.hq || video.original,
      });
    }
  }

  async getBundleChild(serieId) {
    const {bundleInfo} = this.state;
    let step = 0;
    if (bundleInfo.length == 0) {
      step = 1; // first load
    } else if (bundleInfo.length % global.videoEachStep == 0) {
      step = bundleInfo.length / global.videoEachStep + 1; // if length == videoEachStep then load the next array
    }
    if (step != 0) {
      const data = await Network.getBundleChild(serieId, step);
      if (data && data.length) {
        this.setState({bundleInfo: [...bundleInfo, ...data]});
      }
    }
  }

  async getSerieParent(serieId) {
    const data = await Network.getSerieParent(serieId);
    if (data && data.length) {
      this.setState({bundleInfo: data});
    }
  }

  navigateToPayment(paymentData) {
    const {serie} = this.props.route.params;
    if (this.context.user) {
      this.props.navigation.navigate('Payment', {
        serie: serie,
        previousScreen: currentScreen,
        payment: paymentData.period,
        price: paymentData.price,
      });
    } else {
      this.props.navigation.navigate('Login', {
        previousScreen: currentScreen,
      });
    }
  }

  navigateToPaymentSerie(paymentData) {
    const {serie} = this.props.route.params;
    if (this.context.user) {
      this.props.navigation.navigate('PaymentSerie', {
        serie: serie.name,
        previousScreen: 'currentScreen',
        payment: paymentData.period,
        price: paymentData.price,
      });
    } else {
      this.props.navigation.navigate('Login', {
        previousScreen: currentScreen,
      });
    }
  }

  async getSerieVideo() {
    const {serie} = this.props.route.params;
    const {currentSerie} = this.state;
    const data = await Network.getSerieVideo(
      this.context.activeCompany.id,
      serie.id,
      currentSerie.step,
    );
    let step;
    if (data) {
      if (data.length == global.videoEachStep) {
        step = currentSerie.step + 1;
      } else {
        step = currentSerie.step;
      }
    }
    this.setState({
      currentSerie: {id: serie.id, data: data, step: step, loading: false},
    });
  }

  async refresh() {
    const {serie} = this.props.route.params;
    const {currentSerie} = this.state;

    this.setState({
      currentSerie: {data: [], loading: true, id: serie.id, step: 1},
      access: serie.access == 'ok' ? true : false,
      paySerie: false,
      paySubscription: false,
      bundleInfo: [],
    });
    if (serie.trailer) {
      this.getTrailer(serie.trailer);
    }

    if (serie.parent) {
      this.getBundleChild(serie.id);
    } else if (serie.access != 'ok') {
      this.getSerieParent(serie.id);
    }
    if (serie && serie.access !== 'ok') {
      let pSerie = false;
      let pSubscription = false;
      let subscription = {
        price: 0,
        period: 0,
      };
      if (serie.payment.price) {
        pSerie = true;
      }

      if (serie.payment.subscription.length) {
        pSubscription = true;
        subscription = {
          period: serie.payment.subscription[0].period,
          price: serie.payment.subscription[0].price,
        };
      }
      this.setState({
        currentSerie: {...currentSerie, loading: false},
        paySerie: pSerie,
        paySubscription: pSubscription,
        subscription: subscription,
      });
    } else {
      // show serie videos
      this.getSerieVideo();
    }
  }

  viewWillFocus() {
    this.setState({pageHasFocus: true});

    if (this.props.route.params.serie.id != this.state.currentSerie.id) {
      this.refresh();
    }
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () =>
      this.goBack(),
    );
  }

  async goBack() {
    const {previousScreen} = this.props.route.params;
    const {fullscreen} = this.state;
    if (fullscreen) {
      this.toggleTrailer();
    } else {
      if (previousScreen == 'SerieView') {
        // this is the case we are in a pushed SerieView
        this.props.navigation.goBack();
      } else {
        // this is the case this SerieView is the first
        // and we are going to the Catalog
        this.props.navigation.popToTop();
      }
    }
  }

  viewWillBlur() {
    this.setState({pageHasFocus: false});

    this.setState({paused: true, fullscreen: false});
    if (this.backHandler) {
      this.backHandler.remove();
    }
  }

  componentWillUnmount() {
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusListener();
    this.blurListener();
  }

  componentWillMount() {
    this.overlayOpacity = new Animated.Value(1);
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.viewWillFocus(),
    );
    this.blurListener = this.props.navigation.addListener('blur', () =>
      this.viewWillBlur(),
    );
  }

  toggleTrailer() {
    if (!this.state.fullscreen) {
      Orientation.lockToLandscapeLeft();
      setTimeout(() => this.setState({fullscreen: true}), 1000);
    } else {
      this.setState({paused: true, fullscreen: false}, () =>
        Orientation.unlockAllOrientations(),
      );
    }
  }
  async openLink() {
    console.log('param2', await Storage.getToken());
    this.props.navigation.navigate('WebViewPage', {
      source: 'https://teyuto.tv/app/paymentPreview?idc=1577&app=1',
      headers: {
        // Authorization: await Storage.getToken()
      },
    });
  }

  renderHeader() {
    const {serie} = this.props.route.params;
    const {bundleInfo} = this.state;
    if (Platform.isTV) {
      return (
        <View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <ScrollView>
              <AppText
                style={{
                  fontSize: 20,
                  fontWeight: '600',
                  paddingLeft: 10,
                  marginTop: 7,
                }}>
                {serie.title}
              </AppText>
              <AppText
                style={{
                  color: Colors.PlaceholderText,
                  fontSize: 16,
                  fontWeight: '300',
                  paddingLeft: 10,
                  marginBottom: 20,
                }}>
                {serie.description}
              </AppText>
            </ScrollView>
            <ImageBackground
              source={{
                uri:
                  serie.img_preview ||
                  this.context.activeCompany.default_video_image,
              }}
              style={{
                width: global.screenProp.width,
                height:
                  (global.screenProp.width / 16) * 9 -
                  (Platform.isTV ? 100 : 0),
                marginBottom: Platform.isTV ? 10 : 0,
              }}>
              {this.state.trailerSource && (
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1}} />
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <IconPlay
                      name="play"
                      size={50}
                      color={Colors.Text1}
                      style={{opacity: 0.8}}
                      onPress={() => this.toggleTrailer()}
                    />
                  </View>
                </View>
              )}
            </ImageBackground>
          </View>
          <View>
            {!serie.parent && (
              <HomePageList
                keyExtractor={(item, index) => '' + index}
                title={global.strings.SerieIncluded}
                data={bundleInfo}
                onRef={ref => (this.onItemPress = ref)}
                onItemPress={this.openSeries.bind(this)}
                onEndReached={() => this.getBundleChild()}
                onEndReachedThreshold={0.5}
              />
            )}
            {serie.parent && (
              <HomePageList
                keyExtractor={(item, index) => '' + index}
                title={global.strings.BundleContaining}
                data={bundleInfo}
                onRef={ref => (this.onItemPress = ref)}
                onItemPress={this.openSeries.bind(this)}
                style={{flexWrap: 'nowrap'}}
              />
            )}
          </View>
        </View>
      );
    } else {
      return (
        <View>
          <ImageBackground
            source={{
              uri:
                serie.img_preview ||
                this.context.activeCompany.default_video_image,
            }}
            style={{
              width: global.screenProp.width,
              height: (global.screenProp.width / 16) * 9,
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: 'rgba(0,0,0,0.7)',
              }}>
              {this.state.trailerSource && (
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1}} />
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {/*
                    <Icon
                      name="play-arrow"
                      size={50}
                      color={Colors.Text1}
                      style={{ opacity: 0.8 }}
                      onPress={() => this.toggleTrailer()}
                    />
                      */}
                  </View>
                </View>
              )}
            </View>
          </ImageBackground>
          {serie.trailer && (
            <Touchable
              style={styles.boxTitleStyle}
              onPress={() => this.toggleTrailer()}>
              <AppText style={styles.trailerTitleStyle}>
                Visualizza Trailer
              </AppText>
              <Icon
                name="play-arrow"
                size={35}
                color={Colors.Text1}
                style={{opacity: 1}}
              />
            </Touchable>
          )}
          <AppText
            style={{
              fontSize: 20,
              fontWeight: '600',
              paddingLeft: 10,
              marginTop: 7,
            }}>
            {serie.title}
          </AppText>
          <AppText
            style={{
              color: 'rgb(129, 129, 130)',
              fontSize: 16,
              fontWeight: '300',
              paddingLeft: 10,
              marginBottom: 20,
            }}>
            {serie.description}
          </AppText>
          {serie.child && (
            <HomePageList
              title={global.strings.SerieIncluded}
              data={bundleInfo}
              onRef={ref => (this.onItemPress = ref)}
              onItemPress={this.openSeries.bind(this)}
              onEndReached={() => this.getBundleChild()}
              onEndReachedThreshold={0.5}
              style={{width: '100%'}}
            />
          )}
          {serie.parent && (
            <HomePageList
              title={global.strings.BundleContaining}
              data={bundleInfo}
              onRef={ref => (this.onItemPress = ref)}
              onItemPress={this.openSeries.bind(this)}
              style={{width: '100%'}}
            />
          )}
        </View>
      );
    }
  }

  togglePlayer() {
    if (this.overlayOpacity._value == 1) {
      if (this.state.paused) {
        this.setState({paused: false});
      } else {
        this.setState({paused: true});
      }
    }
    this.playerOpacityTimer();
  }

  async fetchMore() {
    const {serie} = this.props.route.params;
    const {currentSerie} = this.state;
    if (currentSerie.data.length % global.videoEachStep == 0) {
      let data = await Network.getSerieVideo(
        this.context.activeCompany.id,
        serie.id,
        currentSerie.step,
      );
      if (data && data.length) {
        let step;
        if (data.length == global.videoEachStep) {
          step = currentSerie.step + 1;
        } else {
          step = currentSerie.step;
        }
        this.setState({
          currentSerie: {
            ...currentSerie,
            data: [...currentSerie.data, ...data],
            step: step,
            loading: false,
          },
        });
      }
    }
  }

  playerOpacityTimer() {
    Animated.timing(this.overlayOpacity, {
      toValue: 1,
      duration: 150,
    }).start(() => {
      if (!this.state.paused) {
        Animated.timing(this.overlayOpacity, {
          toValue: 0,
          duration: 150,
          delay: 3500,
        }).start();
      }
    });
  }

  onError(error) {
    this.setState({errorMessage: error}, () => {
      setTimeout(() => this.setState({errorMessage: ''}), 1800);
    });
  }

  async openSeries(item) {
    const {serie} = this.props.route.params;
    let data;
    this.setState({
      currentSerie: {...this.state.currentSerie, loading: true},
    });
    if (serie.parent) {
      data = await Network.getSerieInfo(item.id_child);
    } else {
      data = await Network.getSerieInfo(item.id_parent);
    }
    this.setState({
      currentSerie: {...this.state.currentSerie, loading: false},
    });
    if (data && data[0]) {
      this.props.navigation.push('SerieView', {
        serie: data[0],
        previousScreen: currentScreen,
      });
    }
  }

  async openPlayer(item) {
    const {currentSerie} = this.state;
    if (item.quiz) {
      this.onError(global.strings.QuizAlert);
    } else {
      // use the same state of the serie for a loading feedback
      this.setState({currentSerie: {...currentSerie, loading: true}});
      const promises = await Promise.all([
        Network.getVideoInfo(item.id),
        Network.getVideoSource(item.id),
        Network.getAttachments(item.id),
      ]);
      const videoInfo = promises[0];
      const data = promises[1];
      const attachments = promises[2];
      let video = formatVideoObject(data);
      if (!videoInfo.ondemand) {
        video.started = videoInfo.livenow;
      }
      // use the same state of the serie for a loading feedback
      this.setState({currentSerie: {...currentSerie, loading: false}});
      this.props.navigation.navigate('Player', {
        previousScreen: currentScreen,
        video: {
          id: item.id,
          creator: item.display_name,
          title: item.title,
          description: item.description,
          source: video,
          attachments: attachments,
        },

        videoInfo: {
          showLike: videoInfo.show_likes,
          showViews: videoInfo.show_views,
          likesTot: videoInfo.likes_tot,
          viewsTot: videoInfo.views,
          liked: !!videoInfo.liked,
          markers: videoInfo.markers,
          productRelated: videoInfo.related_product,
        },
      });
      // this.props.navigation.navigate('Player', { activeCompany: activeCompany, previousScreen: currentScreen, video: { id: item.id, creator: item.display_name, title: item.title, description: item.description, source: video, attachments: attachments }})
    }
  }

  // convertTime(time) {
  //   if(time) {
  //     let date = new Date(1000 * time)
  //     if(time > 3600) {
  //       return date.toISOString().substr(11, 8)
  //     } else {
  //       return date.toISOString().substr(14, 5)
  //     }
  //   } else {
  //     return time
  //   }
  // }

  render() {
    if (this.state.pageHasFocus) {
      const {serie} = this.props.route.params;
      const {
        currentSerie,
        fullscreen,
        paySerie,
        paySubscription,
        subscription,
      } = this.state;
      return (
        <View style={{flex: 1}}>
          {fullscreen && (
            <View style={{flex: 1}}>
              {StatusBar.setHidden(true)}
              <Video
                paused={this.state.paused}
                ref={(ref: Video) => {
                  this.video = ref;
                }}
                onLoad={this.onLoad}
                onProgress={this.onProgress}
                onEnd={this.onEnd}
                source={{uri: this.state.trailerSource}}
                style={{
                  position: 'absolute',
                  width: global.screenProp.height,
                  height: global.screenProp.width,
                }}
                muted={this.state.muted}
                repeat={false}
                resizeMode={'cover'}
                volume={1.0}
                rate={1.0}
                ignoreSilentSwitch={'obey'}
              />

              <Animated.View
                style={[
                  {flex: 1},
                  {
                    opacity: this.overlayOpacity.interpolate({
                      inputRange: [0, 1],
                      outputRange: [0.0, 0.7],
                    }),
                  },
                ]}
                onLayout={event => console.log('')}>
                <Touchable
                  style={{flex: 1, paddingRight: 40}}
                  onPress={() => this.playerOpacityTimer()}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'flex-start',
                      alignItems: 'flex-end',
                      paddingTop: 30,
                    }}>
                    <View
                      style={{
                        backgroundColor: 'red',
                        height: 30,
                        width: 30,
                        borderRadius: 15,
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 3,
                      }}>
                      <Icon
                        name={'clear'}
                        size={20}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                        onPress={() => this.toggleTrailer()}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name={this.state.paused ? 'play-arrow' : 'pause'}
                      size={40}
                      color={
                        this.context.activeCompany.font_color || Colors.Text1
                      }
                      onPress={() => this.togglePlayer()}
                    />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'flex-end',
                      marginBottom: 3,
                      marginRight: 3,
                    }}>
                    <Icon
                      name={this.state.muted ? 'volume-off' : 'volume-up'}
                      size={27}
                      color={
                        this.context.activeCompany.font_color || Colors.Text1
                      }
                      onPress={() => this.setState({muted: !this.state.muted})}
                    />
                  </View>
                </Touchable>
              </Animated.View>
            </View>
          )}
          {!fullscreen && (
            <View style={{flex: 1}}>
              <Background
                header={
                  <Header
                    onCameraPress={() =>
                      this.props.navigation.navigate('ManageLive', {
                        previousScreen: currentScreen,
                      })
                    }
                    onMenuPress={() => this.goBack()}
                    onLogoPress={() => this.props.navigation.popToTop()}
                  />
                }>
                {this.renderHeader()}
                {/* <TouchableOpacity onPress={() =>
                          this.navigateToPaymentSerie({
                            period: 0,
                            price: 200,
                          })
                        }>
                          <View style={{position:'absolute', top:0, right:0,backgroundColor:'red', width:180, height:40}}>

                          <Text style={{color:'#fff'}}>HELLO</Text>
                          </View>
  
</TouchableOpacity> */}
                {/* list */}
                {serie.access == 'ok' && (
                  <FlatList
                    data={currentSerie.data}
                    style={{
                      marginBottom: 20,
                    }}
                    renderItem={({item}) => (
                      <View style={{marginBottom: 22}}>
                        <Touchable
                          style={{
                            margin: 10,
                            flexDirection: 'column',
                            backgroundColor: 'black',
                          }}
                          onPress={() => this.openPlayer(item)}>
                          <ImageBackground
                            imageStyle={{borderRadius: 5}}
                            source={{
                              uri:
                                item.img_preview ||
                                this.context.activeCompany.default_video_image,
                            }}
                            style={[
                              styles.livePreviewStyle,
                              {
                                width: global.screenProp.width - 20,
                                height:
                                  ((global.screenProp.width - 20) / 16) * 9,
                              },
                            ]}>
                            {item.duration ? (
                              <AppText style={styles.liveCollectionStyle}>
                                {item.duration}
                              </AppText>
                            ) : (
                              <AppText />
                            )}
                          </ImageBackground>

                          {Platform.isTV && (
                            <View style={{padding: 16}}>
                              <AppText style={styles.liveTitleStyle}>
                                {item.title}
                              </AppText>
                              <AppText style={styles.liveCollectionStyle}>
                                {item.duration}
                              </AppText>
                            </View>
                          )}
                        </Touchable>

                        <AppText style={styles.liveTitleStyle}>
                          {item.title}
                        </AppText>
                      </View>
                    )}
                    onEndReached={() => this.fetchMore()}
                    keyExtractor={item => String(item.id)}
                    style={styles.flex}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={<View style={{height: 50}} />}
                  />
                )}

                {/* <Touchable onPress={() => this.openLink()} style={{
                  backgroundColor: 'rgb(129,129,129)',
                  width: '30%',
                  padding: 13,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  borderRadius: 10,
                  margin: 8,
                  marginBottom: 60,
                }}></Touchable> */}
                {!fullscreen && serie.access != 'ok' && (
                  <AppText> {global.strings.Access}</AppText>
                )}

                {/*
                {!fullscreen && serie.access != 'ok' && (
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {paySerie && (
                      <TouchableOpacity
                        style={{
                          backgroundColor: Colors.Secondary1,
                          width: '60%',
                          height: 60,
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius: 10,
                          margin: 8,
                        }}
                        // onPress={() =>
                        //   this.navigateToPayment({
                        //     period: 0,
                        //     price: serie.payment.price,
                        //   })
                        // }
                        onPress={() =>
                          this.navigateToPaymentSerie({
                            period: 0,
                            price: serie.payment.price,
                          })
                        }
                      >
                        <AppText
                          style={{
                            color: this.context.activeCompany.font_color || Colors.Text1,
                            fontSize: 16,
                            fontWeight: '600',
                          }}>
                          {global.strings.Pay}
                        </AppText>
                        <AppText style={{ color: this.context.activeCompany.font_color || Colors.Text1, fontSize: 16 }}>
                          {serie.payment.price + global.strings.Euro}
                        </AppText>
                      </TouchableOpacity>
                    )}

                    {paySerie && paySubscription && (
                      <AppText
                        style={{
                          color: this.context.activeCompany.font_color || Colors.Text1,
                          fontSize: 16,
                          padding: 12,
                        }}>
                        {global.strings.Or}
                      </AppText>
                    )}

                    {paySubscription && (
                      <View style={{ width: '100%', alignItems: 'center' }}>
                        <TouchableOpacity
                          style={{
                            backgroundColor: Colors.Secondary1,
                            width: '60%',
                            height: 60,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            margin: 8,
                          }}
                          onPress={() => this.navigateToPaymentSerie(subscription)}>
                          <AppText
                            style={{
                              color: this.context.activeCompany.font_color || Colors.Text1,
                              fontSize: 16,
                              fontWeight: '600',
                            }}>
                            {global.strings.Subscribe}
                          </AppText>
                          <AppText style={{ color: this.context.activeCompany.font_color || Colors.Text1, fontSize: 16 }}>
                            {subscription.price + global.strings.Euro}
                          </AppText>
                        </TouchableOpacity>
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <AppText
                            style={{
                              color: this.context.activeCompany.font_color || Colors.Text1,
                              fontSize: 16,
                              padding: 12,
                            }}>
                            {global.strings.For}
                          </AppText>
                          <Picker
                            itemStyle={{ color: this.context.activeCompany.font_color || Colors.Text1 }}
                            style={[
                              { color: this.context.activeCompany.font_color || Colors.Text1 },
                              Platform.OS === 'android'
                                ? { width: 180 }
                                : { flex: 1 },
                            ]}
                            selectedValue={this.state.subscription.period}
                            onValueChange={(value, index) =>
                              this.setState({
                                subscription: serie.payment.subscription[index],
                              })
                            }>
                            {serie.payment.subscription.map((item, index) => {
                              return (
                                <Picker.Item
                                  label={
                                    String(item.period) +
                                    ' ' +
                                    global.strings.Month
                                  }
                                  value={item.period}
                                  key={index}
                                />
                              );
                            })}
                          </Picker>
                        </View>
                      </View>
                    )}
                  </View>
                )}   */}
              </Background>
              {/* loading modal */}
              <Modal
                visible={currentSerie.loading}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.ModalBackground,
                }}>
                <PulseIndicator
                  size={50}
                  color={this.context.activeCompany.font_color || Colors.Text1}
                />
              </Modal>
            </View>
          )}
        </View>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  livePreviewStyle: {
    height: 163,
    width: 290,
    borderRadius: 5,
    backgroundColor: 'black',
  },
  liveButtonStyle: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  liveDataContainerStyle: {
    flexWrap: 'wrap',
    elevation: 1,
    marginLeft: 8,
    marginRight: 8,
  },
  liveDetailsContainerStyle: {
    marginRight: 4,
    justifyContent: 'center',
  },
  liveTitleStyle: {
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 4,
    marginLeft: 10,
    opacity: 0.7,
  },
  liveCollectionStyle: {
    fontWeight: '700',
    padding: 4,
    backgroundColor: 'rgb(48,48,48)',
    alignSelf: 'flex-end',
    marginTop: 3,
    marginRight: 3,
    fontSize: 14,
    marginBottom: 10,
    color: 'white',
  },
  liveDateStyle: {
    color: Colors.PlaceholderText,
    marginBottom: 2,
    fontSize: 14,
  },
  listFooterStyle: {
    height: 80,
  },
  noLiveTextStyle: {
    color: Colors.Text1,
    marginLeft: 3,
    fontSize: 16,
  },
  spinnerContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  boxTitleStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'rgb(48,48,48)',
    justifyContent: 'center',
  },

  // footer
  buttonIconContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGradientStyle: {
    // position: 'absolute',
    height: 80,
    width: 80,
    alignSelf: 'center',
    borderRadius: 50,
    elevation: 1,
  },
  buttonIconStyle: {
    fontWeight: '600',
    alignSelf: 'center',
  },
  buttonStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  translucentStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 8,
  },
  videoBarStyle: {
    position: 'absolute',
    flexDirection: 'row',
    top: 0,
    right: 0,
    backgroundColor: '#000000',
    borderWidth: 1,
    borderColor: 'red',
    width: 300,
    height: 60,
    padding: 5,
    zIndex: 9999,
  },
  imgStyle: {
    height: 50,
    width: 50,
    alignSelf: 'center',
    margin: 5,
  },

  // modal
  modalWindowContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalWindowStyle: {
    backgroundColor: Colors.Text1,
    borderColor: Colors.Border1,
    borderRadius: 7,
    elevation: 1,
  },
  modalQuestionContainerStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalQuestionStyle: {
    fontSize: 18,
  },
  modalAnswerContainerStyle: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
  },
  modalAnswerTextStyle: {
    borderColor: Colors.Border1,
    width: 150,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
  paymentInput: {
    width: 150,
    height: 40,
    color: Colors.Text1,
    // backgroundColor: 'red',
    fontSize: 18,
    marginTop: Platform.OS == 'ios' ? 8 : 0,
  },

  trailerTitleStyle: {
    textAlignVertical: 'center',
    color: 'white',
    padding: 7,
    fontWeight: 'bold',
    fontSize: 17,
    marginRight: 10,
    textAlign: 'center',
  },

  iconaPlayStyle: {
    textAlign: 'center',
    marginLeft: 10,
    opacity: 0.8,
  },
});

export {SerieView};
