import React from "react";
import { storage } from "react-native-firebase";
import { WebView } from "react-native-webview";
import { Background, Touchable, AppText } from "../components/common";
import Storage from "../services/Storage";
import { StyleSheet, View } from "react-native";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import Icon from "react-native-vector-icons/MaterialIcons";

class WebViewPage extends React.PureComponent {
  static contextType = ActiveCompanyContext;
  state = {
    pageHasFocus: false,
    paramsReady: false,
  };
  webview = null;
  webViewParams: any;
  currentNavigation: any;
  handleClick({}) {}

  componentDidMount() {
    this.getWebViewAuth();
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.setState({ pageHasFocus: true })
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.setState({ pageHasFocus: false })
    );
  }

  async getWebViewAuth() {
    const token = await Storage.getToken();
    this.webViewParams = { uri: this.props.route.params.source };
    if (token) {
      this.webViewParams.headers = { Authorization: token };
    }

    this.setState({ paramsReady: true });
  }

  render() {
    if (this.state.pageHasFocus && this.state.paramsReady) {
      return (
        <>
          <WebView style={{ marginTop: 17 }} source={this.webViewParams} />
          <View style={styles.closeButton}>
            <Touchable
              style={styles.videoBarStyle}
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon
                style={styles.fakeIcon}
                backgroundColor="#3b5998"
                name="arrow-back"
                size={40}
                color="#000"
              />
            </Touchable>
          </View>
        </>
      );
    } else {
      return <Background empty />;
    }
  }
}
const styles = StyleSheet.create({
  closeButton: {
    padding: 2,
    borderRadius: 200,
    backgroundColor: "white",
    position: "absolute",
    top: 40,
    left: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
});
export { WebViewPage };
