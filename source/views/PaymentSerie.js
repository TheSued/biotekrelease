import React from 'react';
import {
    View,
    ScrollView,
    Picker,
    StyleSheet,
    Platform,
    Dimensions,
    BackHandler,
    Alert,
} from 'react-native';

import { PulseIndicator } from 'react-native-indicators';
import CheckBox from 'react-native-check-box';
import stripe, { PaymentCardTextField } from 'tipsi-stripe';

import { ActiveCompanyContext } from '../providers/CompanyProvider';
import * as Network from '../network';
import Colors from '../environments/Colors';
import Countries from '../environments/Countries';

import { Background, AppText, Input, Touchable } from '../components/common';
import { Toast, Modal, Header } from '../components';
import { setStripeEnv } from '../utils/Stripe.utils';
import { formatCustomCartObject } from '../utils/common.utils';

const currentScreen = 'PaymentSerie';

// So -- what my team does is as follows:
// - Hit backend for the right stripe account publishableKey
// - Hit backend for a new SetupIntent (attached to the right stripe publishableKey)
// - Collect card number in mobile app
// - Configure tipsi-stripe with account publishableKey (await)
// - confirmSetupIntent() with card details (await)
// - Receive a paymentMethod from confirmSetupIntent()
// - Send paymentMethodId to backend
// - Backend attaches paymentMethod to customer
// - ... later bill customer using paymentMethod

// check fields when pressing next button
// style on Payment pt2, (
// cards on top, --DONE
// native pay button and text, -- DONE
// pay button change color when native pay confirmed or card form is valid ) -- DONE
// check the formbody of the pay request after
// reset states on blur()

class PaymentSerie extends React.Component {
    static contextType = ActiveCompanyContext;
    state = {
        payment: false,
        valid: false,
        paymentProcessing: false,
        errorMessage: '',
        showPicker: false,
        packSeries: [],
        errors: [],

        // payment details
        cardNameFocus: false,
        addressFocus: false,
        capFocus: false,
        cityFocus: false,
        provinceFocus: false,
        countryFocus: false,
        fullNameFocus: false,
        vatNumFocus: false,
        pecCodeFocus: false,
        mobilePhoneFocus: false,
        emailAddFocus: false,

        country: '',
        fullName: '',
        taxCode: '',
        city: '',
        province: '',
        CAP: '',
        pecCode: '',
        vatNum: '',
        mobilePhone: '',
        emailAdd: '',
        cardName: '',
        address: '',

        couponCode: '',
        couponModal: false,
        giftCardModal: false,
        discountedPrice: null,

        // stripe states
        nativePaymentToken: null,
        canNativePay: false,
        paymentMethod: false,
        pageHasFocus: false,

        customCartData: [],
        showInput: [],
        revalidateInputFields: [],
    };

    revalidateInputs = () => { };

    handleSubmit = () => {
        const {
            city,
            country,
            address,
            CAP,
            cardName,
            fullName,
            taxCode,
            province,
            pecCode,
            vatNum,
            mobilePhone,
            emailAdd,
            customCartData,
            revalidateInputFields,
        } = this.state;

        if (revalidateInputFields.length > 0) {
            Alert.alert('Please remove all errors');
            return;
        }

        let checkValidationFlag = false;

        if (!city && customCartData.cityRequired === true) {
            this.setState({ cityFocus: true });
            checkValidationFlag = true;
        }
        if (!country && customCartData.countryRequired === true) {
            this.setState({ countryFocus: true });
            checkValidationFlag = true;
        }
        // if (!address && false) {
        //   this.setState({addressFocus: true});
        // }
        if (!CAP && customCartData.capRequired === true) {
            this.setState({ capFocus: true });
            checkValidationFlag = true;
        }
        if (!fullName && customCartData.fullnameRequired === true) {
            this.setState({ fullNameFocus: true });
            checkValidationFlag = true;
        }
        if (!taxCode && customCartData.taxRequired === true) {
            this.setState({ taxCodeFocus: true });
            checkValidationFlag = true;
        }
        if (!province && customCartData.provinceRequired === true) {
            this.setState({ provinceFocus: true });
            checkValidationFlag = true;
        }
        if (!pecCode && customCartData.sdicodepecRequired === true) {
            this.setState({ pecCodeFocus: true });
            checkValidationFlag = true;
        }
        if (!vatNum && customCartData.vatnumRequired === true) {
            this.setState({ vatNumFocus: true });
            checkValidationFlag = true;
        }
        if (!mobilePhone && customCartData.phoneRequired === true) {
            this.setState({ mobilePhoneFocus: true });
            checkValidationFlag = true;
        }
        if (!emailAdd && customCartData.emailInvoiceRequired === true) {
            this.setState({ emailAddFocus: true });
            checkValidationFlag = true;
        }
        if (!cardName && customCartData.fullnameRequired === true) {
            this.setState({ cardNameFocus: true });
            checkValidationFlag = true;
        }

        if (!checkValidationFlag) {
            // this.revalidateCustomCartInput();
            Alert.alert(
                'FORM VALIDATED' +
                customCartData.checkboxControlShowInput +
                customCartData.input_show,
            );
        }
    };

    async getPaymentInfo() {
        const data = await Network.getPaymentInfo();
        if (data) {
            setStripeEnv(data.sid);
            this.setState({
                address: data.address,
                CAP: data.cap,
                city: data.city,
                country: data.country,
                cardName: data.name,
                province: data.province,
            });
        } else {
            this.setState({ country: Countries[0].country_code });
        }
    }

    viewWillFocus() {
        this.setState({ pageHasFocus: true, paymentMethod: true });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () =>
            this.goBack(),
        );
        this.getPaymentInfo();
    }

    async goBack() {
        if (this.state.paymentMethod) {
            this.setState({ paymentMethod: false, nativePaymentToken: null });
        }

        if (!this.state.paymentMethod) {
            this.setState({ paymentMethod: true, getPaymentInfo: true });
        } else {
            this.props.navigation.goBack();
        }
    }

    viewWillBlur() {
        this.setState({ pageHasFocus: false });

        if (this.backHandler) {
            this.backHandler.remove();
        }
    }

    componentDidMount() {
        // let inputShow = [];
        this.focusListener = this.props.navigation.addListener('focus', () =>
            this.viewWillFocus(),
        );
        this.blurListener = this.props.navigation.addListener('blur', () =>
            this.viewWillBlur(),
        );
        this.getCustomCartData();
        this.revalidateCustomCartInput();

        // this.setState({showInput:customCartData.checkboxControl.input_show.split(',')})
        // stripe.deviceSupportsNativePay().then((response) => {
        //   this.setState({canNativePay: response});
        // });
    }

    componentWillUnmount() {
        if (this.backHandler) {
            this.backHandler.remove();
        }
        this.focusListener();
        this.blurListener();
    }

    async applyCoupon() {
        const { price } = this.props.route.params;
        const { couponCode } = this.state;

        let response = await Network.checkCouponCode(couponCode);
        if (response) {
            const isGiftcard =
                !!response.is_giftcard ||
                (response.type === 'f' && response.discount === 100);
            this.setState({
                discountedPrice: response.new_price,
                couponModal: false,
                giftCardModal: isGiftcard,
            });
            // success feedback
            this.onError(global.strings.SuccessfullCoupon);
            return true;
        } else {
            // error feedBack
            this.onError(global.strings.InvalidCoupon);
            return false;
        }
    }

    async getClientSecret(clientToken) {
        const { serie, payment } = this.props.route.params;
        const {
            cardName,
            address,
            CAP,
            city,
            province,
            country,
            couponCode,
        } = this.state;
        let networkResponse = await Network.pay({
            clientToken: clientToken, // clientToken
            id: serie.id,
            type: payment,
            couponCode: couponCode,
            cardName: cardName,
            address: address,
            cap: CAP,
            city: city,
            province: province,
            country: country,
        });
        return networkResponse;
    }

    async revalidateCustomCartInput() {
        const {
            city,
            country,
            address,
            CAP,
            cardName,
            fullName,
            taxCode,
            province,
            pecCode,
            vatNum,
            mobilePhone,
            emailAdd,
        } = this.state;
        const { serie } = this.props.route.params;
        const revalidateInput = await Network.getCustomCartInput({
            id: serie.id,
            city: city,
            country: country,
            // address: '',
            fiscalCode: taxCode,
            CAP: CAP,
            cardName: cardName,
            fullName: fullName,
            taxCode: taxCode,
            province: province,
            pecCode: pecCode,
            vatNum: vatNum,
            mobilePhone: mobilePhone,
            emailAdd: emailAdd,
            paymentType: 'create_payment',
            type: 'm',
        });
        if (revalidateInput) {
            this.setState({ revalidateInputFields: revalidateInput });
        }
    }

    async getCustomCartData() {
        const cartInfo = await Network.getCustomCartField();
        let inputShow = [];
        if (cartInfo) {
            let customCart = formatCustomCartObject(cartInfo);
            customCart.checkboxControl.input_show
                .split(',')
                .map((d) => inputShow.push(d));

            this.setState({ customCartData: customCart, showInput: inputShow });
            // console.log('TESTED AND SUCCESS', customCart);
        }
    }

    async confirmPaymentIntent(clientSecret, paymentMethodId) {
        try {
            let stripeResponse = await stripe.confirmPaymentIntent({
                clientSecret: clientSecret,
                paymentMethodId: paymentMethodId,
            });
            if (stripeResponse.status == 'succeeded') {
                return true;
            } else {
                console.log(
                    'confirmPaymentIntentError: payment canceled?',
                    stripeResponse,
                );
                this.onError(global.strings.PaymentCanceled);
                return false;
            }
        } catch (error) {
            console.log('confirmPaymentIntentError: ', error);
            this.onError(global.strings.PaymentDeclined);
            return false;
        }
    }

    async confirmSetupIntent(clientSecret, paymentMethod) {
        let stripeResponse;

        try {
            if (paymentMethod.number) {
                stripeResponse = await stripe.confirmSetupIntent({
                    clientSecret: clientSecret,
                    paymentMethod: {
                        card: {
                            number: paymentMethod.number,
                            cvc: paymentMethod.cvc,
                            expMonth: paymentMethod.expMonth,
                            expYear: paymentMethod.expYear,
                        },
                    },
                });
            } else if (paymentMethod.token) {
                stripeResponse = await stripe.confirmSetupIntent({
                    clientSecret: clientSecret,
                    paymentMethod: {
                        card: {
                            token: paymentMethod,
                        },
                    },
                });
            }
            if (stripeResponse && stripeResponse.status == 'succeeded') {
                return true;
            } else {
                console.log('confirmSetupIntentError: payment canceled');
                this.onError(global.strings.PaymentCanceled);
                return false;
            }
        } catch (error) {
            console.log('confirmSetupIntentError: ', error);
            this.onError(global.strings.PaymentDeclined);
            return false;
        }
    }

    async paymentSuccessfull(param) {
        this.setState({ paymentProcessing: false });
        if (param) {
            this.onError(global.strings.PaymentNotNeeded);
        } else {
            this.onError(global.strings.SuccessfullPayment);
        }
        this.context.togglePaymentSuccessfull();
        setTimeout(() => this.props.navigation.navigate('Catalog'), 2000);
    }

    onError(error) {
        this.setState({ errorMessage: error }, () => {
            setTimeout(() => this.setState({ errorMessage: '' }), 1800);
        });
    }

    async getNativePaymentToken() {
        const { serie, price } = this.props.route.params;
        const { discountedPrice } = this.state;

        const stringPrice = discountedPrice
            ? discountedPrice.toString(10)
            : price.toString(10);
        try {
            let nativeToken = await stripe.paymentRequestWithNativePay({
                total_price: stringPrice,
                currency_code: 'EUR',
                shipping_address_required: false,
                phone_number_required: false,
                // shipping_countries: ['US', 'CA'],
                line_items: [
                    {
                        currency_code: 'EUR',
                        description: serie.title,
                        total_price: stringPrice,
                        unit_price: stringPrice,
                        quantity: '1',
                    },
                ],
            });
            this.setState({ nativePaymentToken: nativeToken });
        } catch (error) {
            console.log('getNativePaymentTokenError: ', error);
            this.onError(global.strings.NativeTokenError);
        }
    }

    async pay() {
        const { serie, payment } = this.props.route.params;
        const { valid, params, nativePaymentToken } = this.state;

        let paymentMethod;
        let stripeResponse;

        this.setState({ paymentProcessing: true });

        if (valid) {
            paymentMethod = await this.createPaymentMethod({ card: params });
        } else if (nativePaymentToken) {
            paymentMethod = await this.createPaymentMethod({
                token: nativePaymentToken,
            });
        } else {
            // missing payment method error
            this.onError(global.strings.InvalidPaymentMethod);
            this.setState({ paymentProcessing: false });
            return 0;
        }
        if (paymentMethod && paymentMethod.id) {
            let clientSecret = await this.getClientSecret(paymentMethod.id);
            if (clientSecret == 1) {
                this.paymentSuccessfull(true);
            } else if (clientSecret) {
                if (payment === 0 || !serie.days_trial) {
                    stripeResponse = await this.confirmPaymentIntent(
                        clientSecret,
                        paymentMethod.id,
                    );
                } else {
                    stripeResponse = await this.confirmSetupIntent(clientSecret, params);
                }
                if (stripeResponse) {
                    this.paymentSuccessfull();
                } else {
                    console.log('payError: bad stripe response', stripeResponse);
                    this.onError(global.strings.PaymentCanceled);
                    this.setState({ paymentProcessing: false });
                    return 0;
                }
            } else {
                console.log('payError: no clientSecret received: ', clientSecret);
                this.onError(global.strings.GeneralError);
                this.setState({ paymentProcessing: false });
                return 0;
            }
        } else {
            console.log('payError: no paymentMethod created');
            this.onError(global.strings.GeneralError);
            this.setState({ paymentProcessing: false });
            return 0;
        }
    }

    async createPaymentMethod(params) {
        const {
            city,
            country,
            address,
            CAP,
            cardName,
            fullName,
            taxCode,
            province,
            pecCode,
            vatNum,
            mobilePhone,
            emailAdd,
        } = this.state;
        try {
            const response = await stripe.createPaymentMethod(
                { card: params.card ? params.card : params.token },
                {
                    billingDetails: {
                        address: {
                            city: city,
                            country: country,
                            line1: address,
                            line2: '',
                            postalCode: CAP,
                            state: '',
                        },
                        email: this.context.user.email,
                        name: cardName,
                        phone: '',
                    },
                },
            );
            if (response) {
                return response;
            } else {
                console.log('createPaymentMethodError: no params?, ', params);
                this.onError(global.strings.GeneralError);
                this.setState({ paymentProcessing: false }); // 0 == no error else different kind of errors
                return 0;
            }
        } catch (error) {
            this.onError(global.strings.GeneralError);
            console.log('createPaymentMethodWithParamsError: ', error, params);
            this.setState({ paymentProcessing: false }); // 0 == no error else different kind of errors
            return 0;
        }
    }

    async createClientToken(params) {
        if (params) {
            try {
                let stripeToken = await stripe.createTokenWithCard({
                    number: params.card.number,
                    cvc: params.card.cvc,
                    expMonth: params.card.expMonth,
                    expYear: params.card.expYear,
                });
                if (stripeToken) {
                    return stripeToken;
                } else {
                    console.log('createClientTokenWithParamsError: ', params);
                    this.onError(global.strings.GeneralError);
                    this.setState({ paymentProcessing: false }); // 0 == no error else different kind of errors
                    return 0;
                }
            } catch (error) {
                console.log('createClientTokenWithParamsError: ', error, params);
                this.onError(global.strings.GeneralError);
                this.setState({ paymentProcessing: false }); // 0 == no error else different kind of errors
                return 0;
            }
        }
    }

    async useGiftCard() {
        const { serie } = this.props.route.params;
        const { couponCode } = this.state;
        const paymentResult = Network.pay({
            id: serie.id,
            type: 'giftcard',
            couponCode,
        });
        if (paymentResult) {
            this.paymentSuccessfull();
        } else {
            this.onError(global.strings.GeneralError);
        }
    }

    showPaymentChoice() {
        // if(cardName !== '' && address !== '' && CAP !== '' && city !== '' && province !== '' && country !== '' && fiscalCode !== '') {
        this.setState({ paymentMethod: true });
        // } else {
        //   this.onError(global.strings.MissingFieldPayment)
        // }
    }

    renderPaymentMethod() {
        const { price } = this.props.route.params;
        const {
            discountedPrice,
            canNativePay,
            valid,
            nativePaymentToken,
            customCartData,
            revalidateInputFields,
        } = this.state;

        console.log('revalidateInputFields', revalidateInputFields);
        return (
            <View style={{ flex: 1 }}>
                {/* native pay button */}
                {/* {canNativePay && (
          <TouchableWithoutFeedback
            onPress={() => this.getNativePaymentToken()}>
            <View
              style={{
                backgroundColor: nativePaymentToken
                  ? Colors.Background2
                  : Colors.Secondary1,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 10,
                padding: 12,
                margin: 8,
              }}>
              <AppText style={{color: Colors.Text1, fontSize: 18}}>
                {global.strings.PayWith}
                {
                  <CommunityIcon
                    name={Platform.OS == 'android' ? 'google' : 'apple'}
                    color={Colors.Text1}
                    size={20}
                  />
                }
              </AppText>
            </View>
          </TouchableWithoutFeedback>
        )} */}

                {/* TESTED DATA */}

                {/* COUNTRY */}
                {this.state.showInput.includes('country') && (
                    <View>
                        <AppText style={styles.textSize}>
                            Country
              {customCartData.countryRequired ? '*' : ''}
                        </AppText>

                        <Input
                            onFocus={() => this.setState({ countryFocus: true })}
                            onBlur={() => this.setState({ countryFocus: false })}
                            underlineColorAndroid={
                                this.state.countryFocus ? Colors.Text1 : Colors.PlaceholderText
                            }
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ country: text })}
                            value={this.state.country}
                            placeholder={''}
                            style={[styles.paymentInput]}
                            paddingLeft={8}
                        />
                        {revalidateInputFields.some((d) => d.input.includes('country')) && (
                            <AppText style={styles.errorTxtMsg}>X Error</AppText>
                        )}
                    </View>
                )}
                {/* <View>
          <AppText style={styles.textSize}> {global.strings.Country} </AppText>
          <Picker
            itemStyle={{color: Colors.Text1}}
            style={Platform.OS == 'android' ? styles.paymentInput : {}}
            selectedValue={this.state.country}
            onValueChange={(value) => this.setState({country: value})}>
            {Countries.map((item, index) => {
              return (
                <Picker.Item
                  label={item.country_name}
                  value={item.country_code}
                  key={index}
                />
              );
            })}
          </Picker>
        </View> */}
                {/* FULL NAME */}
                {this.state.showInput.includes('full_name') && (
                    <View>
                        <AppText style={styles.textSize}>{global.strings.FullName}</AppText>

                        <Input
                            onFocus={() => this.setState({ fullNameFocus: true })}
                            onBlur={() => this.setState({ fullNameFocus: false })}
                            underlineColorAndroid={
                                this.state.fullNameFocus ? Colors.Text1 : Colors.PlaceholderText
                            }
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ fullName: text })}
                            value={this.state.fullName}
                            placeholder={''}
                            style={[styles.paymentInput]}
                            paddingLeft={8}
                            onSubmitEditing={() => this.revalidateCustomCartInput()}
                        />
                        {revalidateInputFields.some((d) =>
                            d.input.includes('fullname'),
                        ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                    </View>
                )}

                <View style={{ flexDirection: 'row' }}>
                    {/* TAX CODE */}
                    {this.state.showInput.includes('fiscal_code') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                Tax Code {customCartData.taxRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ taxCodeFocus: true })}
                                onBlur={() => this.setState({ taxCodeFocus: false })}
                                underlineColorAndroid={
                                    this.state.taxCodeFocus
                                        ? Colors.Text1
                                        : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ taxCode: text })}
                                value={this.state.taxCode}
                                placeholder={''}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />

                            {revalidateInputFields.some((d) =>
                                d.input.includes('fiscal_code'),
                            ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                        </View>
                    )}

                    {/* CITY */}
                    {this.state.showInput.includes('city') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                City {customCartData.cityRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ cityFocus: true })}
                                onBlur={() => this.setState({ cityFocus: false })}
                                underlineColorAndroid={
                                    this.state.cityFocus ? Colors.Text1 : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ city: text })}
                                value={this.state.city}
                                placeholder={''}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />

                            {revalidateInputFields.some((d) => d.input.includes('city')) && (
                                <AppText style={styles.errorTxtMsg}>X Error</AppText>
                            )}
                        </View>
                    )}
                </View>

                <View style={{ flexDirection: 'row' }}>
                    {/* PROVINCE */}
                    {this.state.showInput.includes('province') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                Province {customCartData.provinceRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ provinceFocus: true })}
                                onBlur={() => this.setState({ provinceFocus: false })}
                                underlineColorAndroid={
                                    this.state.provinceFocus
                                        ? Colors.Text1
                                        : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ province: text })}
                                value={this.state.province}
                                placeholder={''}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />

                            {revalidateInputFields.some((d) =>
                                d.input.includes('province'),
                            ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                        </View>
                    )}

                    {/* CAP */}
                    {this.state.showInput.includes('cap') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                CAP {customCartData.capRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ capFocus: true })}
                                onBlur={() => this.setState({ capFocus: false })}
                                underlineColorAndroid={
                                    this.state.capFocus ? Colors.Text1 : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ CAP: text })}
                                value={this.state.CAP}
                                placeholder={''}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />

                            {revalidateInputFields.some((d) => d.input.includes('cap')) && (
                                <AppText style={styles.errorTxtMsg}>X Error</AppText>
                            )}
                        </View>
                    )}
                </View>

                <View style={{ flexDirection: 'row' }}>
                    {/* SDI or PEC Code */}
                    {this.state.showInput.includes('sdi_code_pec') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                SDI or PEC Code {customCartData.sdicodepecRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ pecCodeFocus: true })}
                                onBlur={() => this.setState({ pecCodeFocus: false })}
                                underlineColorAndroid={
                                    this.state.pecCodeFocus
                                        ? Colors.Text1
                                        : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ pecCode: text })}
                                value={this.state.pecCode}
                                placeholder={''}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />

                            {revalidateInputFields.some((d) =>
                                d.input.includes('sdi_code_pec'),
                            ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                        </View>
                    )}

                    {/* VAT NUMBER */}
                    {this.state.showInput.includes('vat_num') && (
                        <View style={styles.customInput}>
                            <AppText style={styles.textSize}>
                                VAT NUMBER {customCartData.vatnumRequired ? '*' : ''}
                            </AppText>
                            <Input
                                onFocus={() => this.setState({ vatNumFocus: true })}
                                onBlur={() => this.setState({ vatNumFocus: false })}
                                underlineColorAndroid={
                                    this.state.vatNumFocus ? Colors.Text1 : Colors.PlaceholderText
                                }
                                autoCapitalize={'words'}
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({ vatNum: text })}
                                value={this.state.vatNum}
                                placeholder={customCartData.vatnumPlaceholder}
                                style={[styles.paymentInput]}
                                paddingLeft={8}
                                onSubmitEditing={() => this.revalidateCustomCartInput()}
                            />
                            {revalidateInputFields.some((d) =>
                                d.input.includes('vat_num'),
                            ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                        </View>
                    )}
                </View>

                {/* Mobile Phone */}
                {this.state.showInput.includes('phone') && (
                    <View>
                        <AppText style={styles.textSize}>
                            Mobile Phone {customCartData.phoneRequired ? '*' : ''}
                        </AppText>
                        <Input
                            onFocus={() => this.setState({ mobilePhoneFocus: true })}
                            onBlur={() => this.setState({ mobilePhoneFocus: false })}
                            underlineColorAndroid={
                                this.state.mobilePhoneFocus
                                    ? Colors.Text1
                                    : Colors.PlaceholderText
                            }
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ mobilePhone: text })}
                            value={this.state.mobilePhone}
                            placeholder={''}
                            style={[styles.paymentInput]}
                            paddingLeft={8}
                            onSubmitEditing={() => this.revalidateCustomCartInput()}
                        />
                        {revalidateInputFields.some((d) => d.input.includes('phone')) && (
                            <AppText style={styles.errorTxtMsg}>X Error</AppText>
                        )}
                    </View>
                )}

                {/* Email address to send invoice */}
                {this.state.showInput.includes('email_for_invoice') && (
                    <View>
                        <AppText style={styles.textSize}>
                            Email address to send invoice{' '}
                            {customCartData.emailInvoiceRequired ? '*' : ''}
                        </AppText>
                        <Input
                            onFocus={() => this.setState({ emailAddFocus: true })}
                            onBlur={() => this.setState({ emailAddFocus: false })}
                            underlineColorAndroid={
                                this.state.emailAddFocus ? Colors.Text1 : Colors.PlaceholderText
                            }
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ emailAdd: text })}
                            value={this.state.emailAdd}
                            placeholder={''}
                            style={[styles.paymentInput]}
                            paddingLeft={8}
                            onSubmitEditing={() => this.revalidateCustomCartInput()}
                        />
                        {revalidateInputFields.some((d) =>
                            d.input.includes('email_for_invoice'),
                        ) && <AppText style={styles.errorTxtMsg}>X Error</AppText>}
                    </View>
                )}
                {/* separator */}
                <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                    <AppText
                        style={{
                            fontSize: 18,
                            fontWeight: '800',
                            width: (Dimensions.get('window').width + 50) / 3,
                        }}>
                        Payment Details
          </AppText>
                    <View
                        style={{
                            width: Dimensions.get('window').width / 2,
                            height: 1,
                            backgroundColor: Colors.PlaceholderText,
                            marginHorizontal: 16,
                            marginVertical: 10,
                            justifyContent: 'center',
                        }}
                    />
                </View>

                <View>
                    <AppText style={styles.textSize}>{global.strings.FullName} </AppText>
                    <Input
                        onFocus={() => this.setState({ cardNameFocus: true })}
                        onBlur={() => this.setState({ cardNameFocus: false })}
                        underlineColorAndroid={
                            this.state.cardNameFocus ? Colors.Text1 : Colors.PlaceholderText
                        }
                        autoCapitalize={'words'}
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({ cardName: text })}
                        value={this.state.cardName}
                        placeholder={''}
                        style={[styles.paymentInput]}
                        paddingLeft={8}
                        onSubmitEditing={() => this.revalidateCustomCartInput()}
                    />
                    {revalidateInputFields.some((d) => d.input.includes('fullname')) && (
                        <AppText style={styles.errorTxtMsg}>X Error</AppText>
                    )}
                </View>
                <View style={{ marginTop: 15 }}>
                    <PaymentCardTextField
                        accessible={false}
                        style={styles.cardInput}
                        onParamsChange={(valid, params) => {
                            this.setState({ valid, params });
                        }}
                        numberPlaceholder="XXXX XXXX XXXX XXXX"
                        expirationPlaceholder="MM/YY"
                        cvcPlaceholder="CVC"
                    />
                </View>

                {/* COMMENTED TO CHECK LATER */}

                <Touchable
                    onPress={() => {
                        discountedPrice
                            ? this.onError(global.strings.DiscountAlreadyApplied)
                            : this.setState({ couponModal: true });
                    }}
                    style={{
                        backgroundColor: discountedPrice
                            ? Colors.PlaceholderText
                            : Colors.Secondary1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 10,
                        padding: 16,
                        margin: 30,
                    }}>
                    <AppText style={{ fontSize: 16, fontWeight: '500' }}>
                        {discountedPrice
                            ? global.strings.DiscountApplied
                            : global.strings.ApplyCoupon}
                    </AppText>
                </Touchable>

                <View
                    style={{
                        backgroundColor: this.context.activeCompany.background_color
                            ? this.context.activeCompany.background_color
                            : Colors.Background2,
                    }}>
                    <Touchable
                        style={{
                            backgroundColor:
                                valid || nativePaymentToken
                                    ? Colors.Secondary1
                                    : Colors.PlaceholderText,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            padding: 12,
                            margin: 8,
                        }}
                        onPress={() => this.handleSubmit()}>
                        <AppText
                            style={{
                                fontSize: 16,
                                fontWeight: '500',
                                textAlign: 'center',
                            }}>
                            {global.strings.Pay +
                                '\n' +
                                (discountedPrice ? discountedPrice : price) +
                                global.strings.Euro}
                        </AppText>
                    </Touchable>
                    <View>
                        {revalidateInputFields.map((d) =>
                            d.tax_rate_valid === true ? (
                                <AppText
                                    style={{
                                        fontSize: 16,
                                        fontWeight: '500',
                                        textAlign: 'center',
                                    }}>
                                    {d.tax_rate_value}
                                    {'%'} VAT / {d.tax_rate_amount_value}
                                    {global.strings.Euro}
                                </AppText>
                            ) : (
                                <></>
                            ),
                        )}
                    </View>
                </View>

                {/* <View
          style={{
            backgroundColor: this.context.activeCompany.background_color
              ? this.context.activeCompany.background_color
              : Colors.Background2,
          }}>
          <Touchable
            style={{
              backgroundColor:
                valid || nativePaymentToken
                  ? Colors.Secondary1
                  : Colors.PlaceholderText,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              padding: 12,
              margin: 8,
            }}
            onPress={() => this.pay()}>
            {/* Bottone pagamento*/}
                {/* <AppText
              style={{
                fontSize: 16,
                fontWeight: '500',
                textAlign: 'center',
              }}>
              {global.strings.Pay +
                '\n' +
                (discountedPrice ? discountedPrice : price) +
                global.strings.Euro}
            </AppText>
          </Touchable>
        </View> */}
            </View>
        );
    }

    renderUserForm() {
        const { discountedPrice } = this.state;
        return (
            <View>
                {/* Coupon button */}

                {/*<Touchable
          onPress={() => {
            discountedPrice ? this.onError(global.strings.DiscountAlreadyApplied) : this.setState({ couponModal: true });
          }}
          style={{
            backgroundColor: discountedPrice ? Colors.PlaceholderText : Colors.Secondary1,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 10,
            padding: 16,
          }}>

          <AppText style={{ fontSize: 16, fontWeight: '500' }}>
            {discountedPrice ? global.strings.DiscountApplied : global.strings.ApplyCoupon}
          </AppText>
        </Touchable> */}

                {/*
        <View>
          <AppText style={styles.textSize}>{global.strings.FullName}</AppText>
          <Input
            onFocus={() => this.setState({ cardNameFocus: true })}
            onBlur={() => this.setState({ cardNameFocus: false })}
            underlineColorAndroid={this.state.cardNameFocus ? Colors.Text1 : Colors.PlaceholderText}
            autoCapitalize={'words'}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ cardName: text })}
            value={this.state.cardName}
            placeholder={''}
            style={[styles.paymentInput]}
            paddingLeft={8}
          />
        </View>
        <View>
          <AppText style={styles.textSize}> {global.strings.Address} </AppText>
          <Input
            onFocus={() => this.setState({ addressFocus: true })}
            onBlur={() => this.setState({ addressFocus: false })}
            underlineColorAndroid={this.state.addressFocus ? Colors.Text1 : Colors.PlaceholderText}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ address: text })}
            value={this.state.address}
            placeholder={''}
            style={[styles.paymentInput]}
            paddingLeft={8}
          />
        </View>
        <View>
          <AppText style={styles.textSize}> {global.strings.CAP} </AppText>
          <Input
            onFocus={() => this.setState({ capFocus: true })}
            onBlur={() => this.setState({ capFocus: false })}
            underlineColorAndroid={this.state.capFocus ? Colors.Text1 : Colors.PlaceholderText}
            keyboardType={'phone-pad'}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ CAP: text })}
            value={this.state.CAP}
            placeholder={''}
            style={[styles.paymentInput]}
            paddingLeft={8}
          />
        </View>
        <View>
          <AppText style={styles.textSize}> {global.strings.City} </AppText>
          <Input
            onFocus={() => this.setState({ cityFocus: true })}
            onBlur={() => this.setState({ cityFocus: false })}
            underlineColorAndroid={this.state.cityFocus ? Colors.Text1 : Colors.PlaceholderText}
            autoCapitalize={'words'}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ city: text })}
            value={this.state.city}
            placeholder={''}
            style={[styles.paymentInput]}
            paddingLeft={8}
          />
        </View>
        <View>
          <AppText style={styles.textSize}> {global.strings.Province} </AppText>
          <Input
            onFocus={() => this.setState({ provinceFocus: true })}
            onBlur={() => this.setState({ provinceFocus: false })}
            underlineColorAndroid={this.state.provinceFocus ? Colors.Text1 : Colors.PlaceholderText}
            autoCapitalize={'characters'}
            autoCorrect={false}
            onChangeText={(text) => this.setState({ province: text })}
            value={this.state.province}
            placeholder={''}
            style={[styles.paymentInput]}
            paddingLeft={8}
          />
        </View>
        <View>
          <AppText style={styles.textSize}> {global.strings.Country} </AppText>
          <Picker
            itemStyle={{ color: Colors.Text1 }}
            style={Platform.OS == 'android' ? styles.paymentInput : {}}
            selectedValue={this.state.country}
            onValueChange={(value) => this.setState({ country: value })}>
            {Countries.map((item, index) => {
              return <Picker.Item label={item.country_name} value={item.country_code} key={index} />;
            })}
          </Picker>
        </View>
        /*}

        {/* Next button */}

                <Touchable
                    style={{
                        backgroundColor: Colors.Secondary1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 10,
                        padding: 16,
                        margin: 8,
                    }}
                    onPress={() => this.showPaymentChoice()}>
                    <AppText style={{ fontSize: 16, fontWeight: '500' }}>
                        {global.strings.Next}
                    </AppText>
                </Touchable>
            </View>
        );
    }

    render() {
        if (this.state.pageHasFocus) {
            const {
                paymentProcessing,
                couponCode,
                couponModal,
                giftCardModal,
                paymentMethod,
            } = this.state;
            return (
                <View style={{ flex: 1 }}>
                    <Background
                        header={
                            <Header
                                onCameraPress={() =>
                                    this.props.navigation.navigate('ManageLive', {
                                        previousScreen: currentScreen,
                                    })
                                }
                                onMenuPress={() => this.goBack()}
                                onLogoPress={() => this.props.navigation.popToTop()}
                            />
                        }>
                        <ScrollView
                            style={{
                                flex: 1,
                                backgroundColor: this.context.activeCompany.background_color
                                    ? this.context.activeCompany.background_color
                                    : Colors.Background2,
                                padding: 16,
                            }}
                            contentContainerStyle={{
                                flexGrow: 1,
                                alignItems: 'stretch',
                                justifyContent: 'center',
                            }}>
                            {paymentMethod && this.renderPaymentMethod()}

                            {!paymentMethod && this.renderUserForm()}
                        </ScrollView>
                    </Background>
                    <Toast message={this.state.errorMessage} />
                    <Modal
                        visible={paymentProcessing || couponModal || giftCardModal}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: Colors.ModalBackground,
                        }}>
                        {paymentProcessing && (
                            <PulseIndicator size={50} color={Colors.Indicator} />
                        )}
                        {couponModal && (
                            <View
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: Colors.ModalColor,
                                    borderRadius: 5,
                                    paddingVertical: 10,
                                    paddingHorizontal: 14,
                                    width: '70%',
                                }}>
                                <Input
                                    underlineColorAndroid={Colors.PlaceholderText}
                                    autoCapitalize={'none'}
                                    autoCorrect={false}
                                    onChangeText={(text) => this.setState({ couponCode: text })}
                                    value={couponCode}
                                    placeholderTextColor={Colors.PlaceholderText}
                                    placeholder={global.strings.DiscountCode}
                                    style={[styles.paymentInput, { width: '90%' }]}
                                    paddingLeft={8}
                                />
                                <Touchable
                                    style={{ padding: 8, marginTop: 8 }}
                                    onPress={() => this.applyCoupon()}>
                                    <AppText
                                        style={{
                                            color: Colors.Text1,
                                            fontWeight: '500',
                                            fontSize: 16,
                                        }}>
                                        {global.strings.Check}
                                    </AppText>
                                </Touchable>
                                <Touchable
                                    style={{ padding: 8, marginTop: 8 }}
                                    onPress={() => this.setState({ couponModal: false })}>
                                    <AppText
                                        style={{
                                            color: Colors.PlaceholderText,
                                            fontSize: 16,
                                        }}>
                                        {global.strings.Cancel}
                                    </AppText>
                                </Touchable>
                            </View>
                        )}
                        {giftCardModal && (
                            <View
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: Colors.ModalColor,
                                    borderRadius: 5,
                                    paddingVertical: 10,
                                    paddingHorizontal: 14,
                                    width: '70%',
                                }}>
                                <AppText
                                    style={{
                                        fontSize: 16,
                                        padding: 4,
                                    }}>
                                    {global.strings.useGiftcard}
                                </AppText>
                                <Touchable
                                    style={{ padding: 8, marginTop: 8 }}
                                    onPress={() => this.useGiftCard()}>
                                    <AppText
                                        style={{
                                            fontWeight: '500',
                                            fontSize: 16,
                                        }}>
                                        {global.strings.Yes}
                                    </AppText>
                                </Touchable>
                                <Touchable
                                    style={{ padding: 8, marginTop: 8 }}
                                    onPress={() => this.setState({ couponModal: false })}>
                                    <AppText
                                        style={{
                                            color: Colors.PlaceholderText,
                                            fontSize: 16,
                                        }}>
                                        {global.strings.No}
                                    </AppText>
                                </Touchable>
                            </View>
                        )}
                    </Modal>
                </View>
            );
        } else {
            return <Background empty />;
        }
    }
}

const styles = StyleSheet.create({
    textSize: {
        fontSize: 18,
        marginTop: 16,
        fontWeight: 'bold',
    },
    errorTxtMsg: {
        fontSize: 14,
        margin: 5,
        color: 'red',
        fontWeight: 'bold',
    },
    customInput: {
        width: (Dimensions.get('window').width - 50) / 2,
        margin: 5,
    },
    paymentInput: {
        fontSize: 18,
        marginTop: Platform.OS == 'ios' ? 8 : 0,
    },
    cardInput: {
        marginHorizontal: 8,
        color: '#449aeb',
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        overflow: 'hidden',
    },
});

export { PaymentSerie };
