import 'react-native-gesture-handler';
import React from 'react';
import {Dimensions, Platform} from 'react-native';

import {AppProvider} from './source/providers';
import Navigator from './source/Navigator';
import defineLanguage from './source/environments/languages/Language';

global.videoEachStep = 9;
global.strings = defineLanguage();
global.font = Platform.OS == 'android' ? 'Ubuntu-R' : 'Ubuntu';

// without this, if the app starts in landscape orientation the streamview will bug
let param = {
  w: Dimensions.get('screen').width,
  h: Dimensions.get('screen').height,
};
global.screenProp = {
  width: param.w < param.h ? param.w : param.h,
  height: param.w < param.h ? param.h : param.w,
};
// end comment

class App extends React.Component {
  render() {
    return (
      <AppProvider>
        <Navigator />
      </AppProvider>
    );
  }
}

export default App;

// Live slider with live duration / offset from current time, conditional dot red / grey -- Monday
// GoogleCast -- Monday

// ComponentWillMount
// ScrollView + Flatlist

// update to use createAnimatedSwitchNavigator()

// Video element props
// smoothSkinLevel: 0 - 5
// denoise: bool -- mute?
// video: { preset: any number?, profile: 0 - 2 }

// Video presets
// public static final int VIDEO_PPRESET_16X9_270 = 0;
// public static final int VIDEO_PPRESET_16X9_360 = 1;
// public static final int VIDEO_PPRESET_16X9_480 = 2;
// public static final int VIDEO_PPRESET_16X9_540 = 3;
// public static final int VIDEO_PPRESET_16X9_720 = 4;

// Video profiles
// public static final int VIDEO_PROFILE_BASELINE = 0;
// public static final int VIDEO_PROFILE_MAIN = 1;
// public static final int VIDEO_PROFILE_HIGH = 2;
